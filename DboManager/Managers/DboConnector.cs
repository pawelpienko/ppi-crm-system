﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using DboManager.Helpers;
using DboManager.Models;

namespace DboManager.Managers
{
    public static class DboConnector
    {
        private const string ServerName = @".\SQLEXPRESS";
        private const string DatabaseName = "CRM_SYSTEM_DBO";
        private static SqlConnection _dbConnection;

        public static SqlConnection DbConnection => _dbConnection ?? (_dbConnection = new SqlConnection(GetConnectionString()));

        public static void ExecuteCreationScripts()
        {
            var allScripts = FilesHelper.GetScriptsFullNames();
            foreach (var scriptPath in allScripts)
            {
                ExecuteFileScript(scriptPath);
            }
        }

        public static bool ExecuteProcedure(string procedureName, List<ParameterModel> parameters)
        {
            LogManager.Log($"Starting executing procedure {procedureName}");
            try
            {
                DbConnection.Open();
                using (var command = new SqlCommand(procedureName, DbConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (var param in parameters)
                    {
                        var currentParam = new SqlParameter
                        {
                            ParameterName = param.ParameterName,
                            SqlDbType = param.SqlDbType,
                            Size = param.Size,
                            Direction = param.Direction
                        };

                        if (param.SqlDbType == SqlDbType.Int)
                        {
                            int.TryParse(param.Value, out int intResult);
                            currentParam.Value = intResult;
                        }
                        else
                        {
                            currentParam.Value = param.Value;
                        }

                        command.Parameters.Add(currentParam);
                    }
                    
                    command.ExecuteNonQuery();
                    LogManager.Log($"Procedure {procedureName} executed.");
                    var result = command.Parameters[parameters.Count - 1];
                    
                    return result.Value.Equals("1");
                }
            }
            catch (Exception ex)
            {
                LogManager.Log($"Unexpected error occured during executing stored procedure {procedureName}: {ex}", LogLevel.Error);
                return false;
            }
            finally
            {
                DbConnection?.Close();
            }
        }

        private static string GetConnectionString()
        {
            return $@"Data Source={ServerName};Initial Catalog={DatabaseName};Integrated Security=True";
        }

        private static void ExecuteFileScript(string scriptDestinationPath)
        {
            if (!File.Exists(scriptDestinationPath))
            {
                LogManager.Log($"File: {scriptDestinationPath} does not exist.", LogLevel.Error);
                return;
            }

            try
            {
                var script = File.ReadAllText(scriptDestinationPath, Encoding.UTF8);
                LogManager.Log("Connecting to database");
                DbConnection.Open();
                using (var command = new SqlCommand(script, DbConnection))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                LogManager.Log($"Unexpected error occured during executing script: {ex}", LogLevel.Error);
            }
            finally
            {
                DbConnection?.Close();
                LogManager.Log($"File: {scriptDestinationPath} processed.", LogLevel.Debug);
            }
        }
    }
}