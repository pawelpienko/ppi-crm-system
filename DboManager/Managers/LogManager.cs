﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using DboManager.Helpers;

namespace DboManager.Managers
{
    public static class LogManager
    {
        private static string _logFile;

        public static void Log(string value, LogLevel logLevel = LogLevel.Debug)        
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }

            try
            {
                using (var writer = new StreamWriter(LogFile, true))
                {
                    writer.WriteAsync(PrepareLogLine(value, logLevel));
                }
            }
            catch (Exception ex)
            { }
            
        }

        private static string LogFile
        {
            get
            {
                if (string.IsNullOrEmpty(_logFile))
                {
                    _logFile = GetCurrentLogsFile();
                }

                return _logFile;
            }
        }

        private static string GetCurrentLogsFile()
        {
            const string directoryName = "Logs";
            var dir = FilesHelper.SolutionDirectoryInfo.GetDirectories().FirstOrDefault(n=>n.Name.Equals(directoryName));
            string path;

            if (dir == null)
            {
                path = $@"{FilesHelper.SolutionDirectoryInfo.FullName}\{directoryName}";
                Directory.CreateDirectory(path);
            }
            else
            {
                path = dir.FullName;
                var logFiles = dir.GetFiles().Where(n => n.Extension.Equals(".log")).ToList();
                if (logFiles.Count > 0)
                {
                    var orderedFiles = logFiles.OrderByDescending(n => n.CreationTime);
                    var latestLogFile = orderedFiles.First();

                    if (latestLogFile != null && latestLogFile.Length < 10000)
                    {
                        return latestLogFile.FullName;
                    }
                }
            }

            var currentLogFilePath = $@"{path}\{DateTime.Now:yyyyMMddHHmmss}.log";
            File.Create(currentLogFilePath).Close();

            return currentLogFilePath;
        }

        private static string PrepareLogLine(string value, LogLevel logLevel)
        {
            return $"{DateTime.Now:ddMMyyyyhhmmssfff}|{Thread.CurrentThread.ManagedThreadId}|{logLevel.ToString()}|{value}{Environment.NewLine}";
        }
    }

    public enum LogLevel
    {
        Debug,
        Error
    }
}