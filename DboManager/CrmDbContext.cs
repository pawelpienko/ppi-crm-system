﻿using System.Data.Entity;
using DboManager.DboModels;

namespace DboManager
{
    public class CrmDbContext : DbContext
    {
        public CrmDbContext() : base(@"Data Source=sql5059.site4now.net;Initial Catalog=DB_A6284D_PawelP82274;Persist Security Info=True;User ID=DB_A6284D_PawelP82274_admin;Password=Gino3_Rossi00")
        {
            
        }

        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<ContactPerson> ContactPersons { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet <Event> Events { get; set; }
        public DbSet<Task> Taks { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<User> Users { get; set; }
    }
}