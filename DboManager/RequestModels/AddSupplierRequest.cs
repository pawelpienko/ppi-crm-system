﻿using System.Runtime.Serialization;
using DboManager.Models;

namespace DboManager.RequestModels
{
    [DataContract]
    public class AddSupplierRequest
    {
        [DataMember]
        public WCFSupplier Supplier { get; set; } 
    }

    public class RemoveSupplierRequest 
    {
        [DataMember]
        public int SupplierId { get; set; }
    }
}