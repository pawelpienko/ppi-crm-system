﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DboManager.DboModels
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual List<Event> Events { get; set; }
    }
}