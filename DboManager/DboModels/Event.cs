﻿using System;
using System.Collections.Generic;

namespace DboManager.DboModels
{
    public class Event
    {
        public int EventId { get; set; }
        public string EventTitle { get; set; }
        public DateTime EventExecutionDate { get; set; }
        public DateTime EventCreationDate { get; set; }
        public virtual List<Task> Tasks { get; set; }
        public EventStatus Status { get; set; }
        public string Description { get; set; }
        public EventType EventType { get; set; }
        public Address EventAddress { get; set; }
        public virtual Customer EventCustomer { get; set; }
        public virtual List<Product> Products { get; set; }
    }

    public enum EventStatus
    {
        Reported,
        UnderAnalysis,
        InProgress,
        Done
    }

    public enum EventType
    {
        Picnic,
        Jubilee,
        Conference,
        Opening,
        ThemeParty,
        Integration,
        Others
    }
}