﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DboManager.DboModels
{
    public class Supplier
    {
        public int SupplierId { get; set; }
        public string SupplierCompanyName { get; set; }
        public virtual Address Address { get; set; }
        public virtual ContactPerson ContactPerson { get; set; }
        public string Details { get; set; }
        public virtual List<Product> SupplierProducts { get; set; }
        public DateTime CreationDate { get; set; }
    }
}