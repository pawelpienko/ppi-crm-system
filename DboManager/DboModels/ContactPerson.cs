﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DboManager.DboModels
{
    public class ContactPerson
    {
        public int ContactPersonId { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string Phone { get; set; }
        public virtual Professions Profession { get; set; }
    }

    public enum Professions
    {
        Programmer,
        HrSpecialist,
        DatabaseMaster,
        Manager,
        Other
    }
}