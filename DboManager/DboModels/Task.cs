﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DboManager.DboModels
{
    public class Task
    {
        public int TaskId { get; set; }
        public string Title { get; set; }
        public string TaskDescription { get; set; }
        public bool Finished { get; set; }
        public virtual Event TaskEvent { get; set; }
    }
}