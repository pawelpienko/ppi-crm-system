﻿using System;

namespace DboManager.DboModels
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string CompanyName { get; set; }
        public virtual Address Address { get; set; }
        public virtual ContactPerson ContactPerson { get; set; }
        public string Details { get; set; }
        public DateTime CreationDate { get; set; }
    }
}