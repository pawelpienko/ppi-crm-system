﻿using System;

namespace DboManager.DboModels
{
    public class User
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public DateTime CreationDate { get; set; }
        public UserRoles Role { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public virtual Address UserAddress { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Professions Profession { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
    }

    public enum UserRoles
    {
        Administrator,
        RegularUser
    }
}