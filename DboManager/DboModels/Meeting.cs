﻿using System;

namespace DboManager.DboModels
{
    public class Meeting
    {
        public int MeetingId { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }
}