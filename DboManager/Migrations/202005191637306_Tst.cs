﻿namespace DboManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tst : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        Street = c.String(),
                        HomeNo = c.String(),
                        HomeExt = c.String(),
                        ZipCode = c.String(),
                        City = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.AddressId);
            
            CreateTable(
                "dbo.ContactPersons",
                c => new
                    {
                        ContactPersonId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SecondName = c.String(),
                        Phone = c.String(),
                        Profession = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ContactPersonId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                        SupplierId = c.Int(nullable: false),
                        NamePriceFormatted = c.String(),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        SupplierId = c.Int(nullable: false, identity: true),
                        SupplierCompanyName = c.String(),
                        Details = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        Address_AddressId = c.Int(),
                        ContactPerson_ContactPersonId = c.Int(),
                    })
                .PrimaryKey(t => t.SupplierId)
                .ForeignKey("dbo.Addresses", t => t.Address_AddressId)
                .ForeignKey("dbo.ContactPersons", t => t.ContactPerson_ContactPersonId)
                .Index(t => t.Address_AddressId)
                .Index(t => t.ContactPerson_ContactPersonId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.Suppliers", "ContactPerson_ContactPersonId", "dbo.ContactPersons");
            DropForeignKey("dbo.Suppliers", "Address_AddressId", "dbo.Addresses");
            DropIndex("dbo.Suppliers", new[] { "ContactPerson_ContactPersonId" });
            DropIndex("dbo.Suppliers", new[] { "Address_AddressId" });
            DropIndex("dbo.Products", new[] { "SupplierId" });
            DropTable("dbo.Suppliers");
            DropTable("dbo.Products");
            DropTable("dbo.ContactPersons");
            DropTable("dbo.Addresses");
        }
    }
}
