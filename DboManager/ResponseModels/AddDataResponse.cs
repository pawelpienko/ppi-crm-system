﻿using System.Runtime.Serialization;

namespace DboManager.ResponseModels
{
    [DataContract]
    public class AddDataResponse
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public string Message { get; set; }

        public AddDataResponse(bool success, string message)
        {
            Success = success;
            Message = message;
        }
    }
}