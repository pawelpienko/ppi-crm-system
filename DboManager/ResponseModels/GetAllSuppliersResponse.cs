﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using DboManager.Models;

namespace DboManager.ResponseModels
{
    [DataContract]
    public class GetAllSuppliersResponse
    {
        [DataMember]
        public List<WCFSupplier> WCFSuppliers { get; set; }
    }

    [DataContract]
    public class DeleteDataResponse
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string Message { get; set; }

    }
}