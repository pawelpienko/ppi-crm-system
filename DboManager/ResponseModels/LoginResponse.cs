﻿using System.Runtime.Serialization;
using DboManager.Models;

namespace DboManager.ResponseModels
{
    [DataContract]
    public class LoginResponse: AddDataResponse
    {
        [DataMember]
        public WCFUser WCFUser { get; set; }
        public LoginResponse(bool success, string message, WCFUser wcfUser) : base(success, message)
        {
            Message = message;
            Success = success;
            WCFUser = wcfUser;
        }
    }
}