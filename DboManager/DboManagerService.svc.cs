﻿using System;
using System.Collections.Generic;
using System.Data;
using DboManager.DboModels;
using DboManager.Helpers;
using DboManager.Managers;
using DboManager.Models;
using DboManager.RequestModels;
using DboManager.ResponseModels;

namespace DboManager
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DboManagerService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DboManagerService.svc or DboManagerService.svc.cs at the Solution Explorer and start debugging.
    public class DboManagerService : IDboManagerService
    {
        public void ExecuteCreationScripts()
        {
            DboConnector.ExecuteCreationScripts();
        }

        public bool InsertNewAddress(AddressModel address)
        {
            var parameters = new List<ParameterModel>
            {
                new ParameterModel("@HomeNo","3", SqlDbType.NVarChar,8),
                new ParameterModel("@HomeExt","B", SqlDbType.NVarChar,8),
                new ParameterModel("@Street","Buchalowska", SqlDbType.NVarChar,50),
                new ParameterModel("@City",address.City, SqlDbType.NVarChar,40),
                new ParameterModel("@Country","Poland", SqlDbType.NVarChar,40),
                new ParameterModel("@Details","Kolo somsiadow w lewo", SqlDbType.NVarChar,250),
                new ParameterModel("@Result","", SqlDbType.Int,8, ParameterDirection.Output) 
            };

            return DboConnector.ExecuteProcedure("PR_InsertNewAddress_01", parameters);
        }

        public AddDataResponse AddEvent(WCFEvent wcfEvent)
        {
            return new DboHelper().AddEvent(wcfEvent);
        }

        public void DeleteEvent(int eventId)
        {
            new DboHelper().DeleteEvent(eventId);
        }

        public List<WCFEvent> GetAllEvents()
        {
            return new DboHelper().GetAllEvents();
        }

        public AddDataResponse AddCustomer(WCFCustomer customer)
        {
            return new DboHelper().AddCustomer(customer);
        }

        public List<WCFCustomer> GetAllCustomers()
        {
            return new DboHelper().GetAllCustomers();
        }

        public DeleteDataResponse DeleteCustomer(int customerId)
        {
            return new DboHelper().DeleteCustomer(customerId);
        }

        public AddDataResponse AddNewSupplier(AddSupplierRequest request)
        {
            var response = new DboHelper().AddSupplier(request.Supplier);
            return response;
        }

        public AddDataResponse UpdateSupplier(WCFSupplier supplier)
        {
            var response = new DboHelper().UpdateSupplier(supplier);
            return response;
        }

        public GetAllSuppliersResponse GetAllSuppliers()
        {
            return new DboHelper().GetAllSuppliers();
        }

        public DeleteDataResponse DeleteSupplier(int supplierId)
        {
            return new DboHelper().DeleteSupplier(supplierId);
        }

        public AddDataResponse AddMeeting(WCFMeeting wcfMeeting)
        {
            return new DboHelper().AddMeeting(wcfMeeting);
        }

        public DeleteDataResponse RemoveMeeting(int meetingId)
        {
            return new DboHelper().RemoveMeeting(meetingId);
        }

        public List<WCFMeeting> GetAllMeetings()
        {
            return new DboHelper().GetAllMeetings();
        }

        public LoginResponse LogIn(string login, string password)
        {
            return new LoginHelper().LogIn(login, password);
        }

        public LoginResponse AddUser(WCFUser wcfUser)
        {
            return new LoginHelper().AddUser(wcfUser);
        }
    }
}
