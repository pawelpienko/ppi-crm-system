﻿using System.Data;

namespace DboManager.Models
{
    public class ParameterModel
    {
        public string ParameterName { get; set; }

        public string Value { get; set; }

        public SqlDbType SqlDbType { get; set; }

        public int Size { get; set; }

        public ParameterDirection Direction { get; set; }

        public ParameterModel(string parameterName, string value, SqlDbType sqlDbType, int size, ParameterDirection direction = ParameterDirection.Input)
        {
            ParameterName = parameterName;
            SqlDbType = sqlDbType;
            Value = value;
            Size = size;
            Direction = direction;
        }
    }
}