﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using DboManager.DboModels;

namespace DboManager.Models
{
    [DataContract]
    public class WCFSupplier
    {
        [DataMember]
        public int SupplierId { get; set; }
        
        [DataMember]
        public string SupplierCompanyName { get; set; }

        [DataMember]
        public WCFAddress Address { get; set; }

        [DataMember]
        public WCFContactPerson ContactPerson { get; set; }

        [DataMember]
        public string Details { get; set; }

        [DataMember]
        public virtual List<WCFProduct> SupplierProducts { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }
    }

    [DataContract(IsReference = true)]
    public class WCFProduct
    {
        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public virtual WCFSupplier Supplier { get; set; }

        //[DataMember]
        //public virtual List<WCFEvent> Events { get; set; }
    }

    [DataContract]
    public class WCFAddress
    {
        [DataMember]
        public int AddressId { get; set; }

        [DataMember]
        public string Street { get; set; }

        [DataMember]
        public string HomeNo { get; set; }

        [DataMember]
        public string HomeExt { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Country { get; set; }
    }

    [DataContract]
    public class WCFContactPerson
    {
        [DataMember]
        public int ContactPersonId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string SecondName { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public Professions Profession { get; set; }
    }
}