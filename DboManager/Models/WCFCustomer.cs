﻿using System;
using System.Runtime.Serialization;

namespace DboManager.Models
{
    [DataContract]
    public class WCFCustomer
    {
        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public string CompanyName { get; set; }

        [DataMember]
        public WCFAddress Address { get; set; }

        [DataMember]
        public WCFContactPerson ContactPerson { get; set; }

        [DataMember]
        public string Details { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }
    }
}