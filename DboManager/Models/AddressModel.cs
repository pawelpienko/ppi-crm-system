﻿namespace DboManager.Models
{
    public class AddressModel
    {
        //TODO[DataContract] pododawac!
        public string HomeNo { get; set; }
        public string HomeExt { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Details { get; set; }
    }
}