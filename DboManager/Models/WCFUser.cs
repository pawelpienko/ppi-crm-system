﻿using System;
using System.Runtime.Serialization;
using DboManager.DboModels;

namespace DboManager.Models
{
    [DataContract]
    public class WCFUser
    {
        [DataMember]
        public int WcfUserId { get; set; }
        [DataMember]
        public string Login { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public DateTime CreationDate { get; set; }
        [DataMember]
        public UserRoles Role { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string SecondName { get; set; }
        [DataMember]
        public WCFAddress UserAddress { get; set; }
        [DataMember]
        public DateTime DateOfBirth { get; set; }
        [DataMember]
        public Professions Profession { get; set; }
        [DataMember]
        public string PhoneNo { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}