﻿using System;
using System.Collections.Generic;
using DboManager.DboModels;

namespace DboManager.Models
{
    public class WCFEvent
    {
        public int EventId { get; set; }
        public string EventTitle { get; set; }
        public DateTime EventExecutionDate { get; set; }
        public DateTime EventCreationDate { get; set; }
        public List<WCFTask> Tasks { get; set; }
        public EventStatus Status { get; set; }
        public string Description { get; set; }
        public EventType EventType { get; set; }
        public WCFAddress EventAddress { get; set; }
        public WCFCustomer EventCustomer { get; set; }
        public List<WCFProduct> Products { get; set; }
    }

    public class WCFTask
    {
        public int TaskId { get; set; }
        public string Title { get; set; }
        public string TaskDescription { get; set; }
        public bool Finished { get; set; }
    }
}