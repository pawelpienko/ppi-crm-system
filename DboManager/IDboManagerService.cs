﻿using System.Collections.Generic;
using System.ServiceModel;
using DboManager.Models;
using DboManager.RequestModels;
using DboManager.ResponseModels;

namespace DboManager
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IDboManagerService
    {
        [OperationContract]
        void ExecuteCreationScripts();

        [OperationContract]
        bool InsertNewAddress(AddressModel address);

        #region Events
        [OperationContract]
        AddDataResponse AddEvent(WCFEvent wcfEvent);

        [OperationContract]
        void DeleteEvent(int eventId);

        [OperationContract]
        List<WCFEvent> GetAllEvents();

        #endregion

        #region Customers
        [OperationContract]
        AddDataResponse AddCustomer(WCFCustomer customer);
        
        [OperationContract]
        List<WCFCustomer> GetAllCustomers();

        [OperationContract]
        DeleteDataResponse DeleteCustomer(int customerId);

        #endregion

        #region Suppliers

        [OperationContract]
        AddDataResponse AddNewSupplier(AddSupplierRequest request);

        [OperationContract]
        AddDataResponse UpdateSupplier(WCFSupplier supplier);

        [OperationContract]
        GetAllSuppliersResponse GetAllSuppliers();

        [OperationContract]
        DeleteDataResponse DeleteSupplier(int supplierId);

        #endregion

        #region Meetings

        [OperationContract]
        AddDataResponse AddMeeting(WCFMeeting wcfMeeting);

        [OperationContract]
        DeleteDataResponse RemoveMeeting(int meetingId);

        [OperationContract]
        List<WCFMeeting> GetAllMeetings();

        [OperationContract]
        LoginResponse LogIn(string login, string password);

        [OperationContract]
        LoginResponse AddUser(WCFUser wcfUser);

        #endregion
    }
}
