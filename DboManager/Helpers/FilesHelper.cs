﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace DboManager.Helpers
{
    public class FilesHelper
    {
        private static DirectoryInfo _solutionDirectoryInfo;

        public static DirectoryInfo SolutionDirectoryInfo => _solutionDirectoryInfo ?? (_solutionDirectoryInfo = TryGetSolutionDirectoryInfo());

        public static List<string> GetScriptsFullNames()
        {
            var files = TryGetSolutionDirectoryInfo().Parent;
            return files == null ? null : GetScriptFullNames(files.FullName);
        }

        private static List<string> GetScriptFullNames(string rootPath)
        {
            var dir = new DirectoryInfo(rootPath);
            var scriptsDirectory = dir.GetDirectories("Database*").FirstOrDefault()?.GetDirectories("CreateScripts*").FirstOrDefault();
            
            return scriptsDirectory?.GetFiles().Where(n=>n.Extension.ToLower().Equals(".sql")).Select(n => n.FullName).ToList();
        }

        private static DirectoryInfo TryGetSolutionDirectoryInfo()
        {
            var uri = new Uri(Assembly.GetExecutingAssembly().GetName().CodeBase);
            var loadPath = Path.GetDirectoryName(uri.LocalPath);
            var directory = new DirectoryInfo(loadPath);

            while (directory != null && !directory.GetFiles("*.sln").Any())
            {
                directory = directory.Parent;
            }
            return directory;
        }
    }
}