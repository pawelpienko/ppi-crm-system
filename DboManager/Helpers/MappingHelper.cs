﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DboManager.DboModels;
using DboManager.Models;

namespace DboManager.Helpers
{
    public static class MappingHelper
    {
        internal static WCFCustomer MapCustomer(this Customer customer)
        {
            if (customer == null)
            {
                return null;
            }

            var address = customer.Address ?? new Address();
            var contactPerson = customer.ContactPerson ?? new ContactPerson();
            return new WCFCustomer
            {
                CustomerId = customer.CustomerId,
                CompanyName = customer.CompanyName,
                Address = new WCFAddress
                {
                    AddressId = address.AddressId,
                    City = address.City,
                    Country = address.Country,
                    HomeExt = address.HomeExt,
                    HomeNo = address.HomeNo,
                    Street = address.Street,
                    ZipCode = address.ZipCode
                },
                ContactPerson = new WCFContactPerson
                {
                    ContactPersonId = contactPerson.ContactPersonId,
                    Name = contactPerson.Name,
                    Phone = contactPerson.Phone,
                    Profession = contactPerson.Profession,
                    SecondName = contactPerson.SecondName
                },
                CreationDate = customer.CreationDate,
                Details = customer.Details
            };
        }

        internal static Customer MapCustomer(this WCFCustomer wcfCustomer, CrmDbContext session)
        {
            if (wcfCustomer == null)
            {
                return null;
            }

            var myCustomer = session.Customers.FirstOrDefault(n=>n.CustomerId == wcfCustomer.CustomerId) ?? new Customer
            {
                CompanyName = wcfCustomer.CompanyName,
                CreationDate = DateTime.Now,
                Details = wcfCustomer.Details
            };

            var wcfAddress = wcfCustomer.Address;
            if (wcfAddress != null)
            {
                myCustomer.Address = session.Addresses.FirstOrDefault(n => n.AddressId == wcfAddress.AddressId) ?? new Address();
                myCustomer.Address.AddressId = wcfAddress.AddressId;
                myCustomer.Address.City = wcfAddress.City;
                myCustomer.Address.Country = wcfAddress.Country;
                myCustomer.Address.HomeExt = wcfAddress.HomeExt;
                myCustomer.Address.HomeNo = wcfAddress.HomeNo;
                myCustomer.Address.Street = wcfAddress.Street;
                myCustomer.Address.ZipCode = wcfAddress.ZipCode;
            }
            
            var wcfContactPerson = wcfCustomer.ContactPerson;
            if (wcfContactPerson != null)
            {
                myCustomer.ContactPerson = session.ContactPersons.FirstOrDefault(n => n.ContactPersonId == wcfContactPerson.ContactPersonId) ?? new ContactPerson();
                myCustomer.ContactPerson.ContactPersonId = wcfContactPerson.ContactPersonId;
                myCustomer.ContactPerson.Name = wcfContactPerson.Name;
                myCustomer.ContactPerson.Phone = wcfContactPerson.Phone;
                myCustomer.ContactPerson.Profession = wcfContactPerson.Profession;
                myCustomer.ContactPerson.SecondName = wcfContactPerson.SecondName;
            }

            return myCustomer;
        }

        internal static void MapSupplier(this WCFSupplier wcfSupplier, CrmDbContext session, ref Supplier mySupplier)
        {
            mySupplier.SupplierCompanyName = wcfSupplier.SupplierCompanyName;
            mySupplier.SupplierId = wcfSupplier.SupplierId;
            mySupplier.Details = wcfSupplier.Details;
            mySupplier.CreationDate = wcfSupplier.CreationDate;

            var address = wcfSupplier.Address;
            if (address != null)
            {
                mySupplier.Address = session.Addresses.FirstOrDefault(n => n.AddressId == address.AddressId) ?? new Address();
                mySupplier.Address.AddressId = address.AddressId;
                mySupplier.Address.City = address.City;
                mySupplier.Address.Country = address.Country;
                mySupplier.Address.HomeExt = address.HomeExt;
                mySupplier.Address.Street = address.Street;
                mySupplier.Address.HomeNo = address.HomeNo;
                mySupplier.Address.ZipCode = address.ZipCode;
            }

            var contactPerson = wcfSupplier.ContactPerson;
            if (contactPerson != null)
            {
                mySupplier.ContactPerson = session.ContactPersons.FirstOrDefault(n => n.ContactPersonId == contactPerson.ContactPersonId) ?? new ContactPerson();
                mySupplier.ContactPerson.ContactPersonId = contactPerson.ContactPersonId;
                mySupplier.ContactPerson.Name = contactPerson.Name;
                mySupplier.ContactPerson.Phone = contactPerson.Phone;
                mySupplier.ContactPerson.Profession = contactPerson.Profession;
                mySupplier.ContactPerson.SecondName = contactPerson.SecondName;
            }

            if (mySupplier.SupplierProducts == null)
            {
                mySupplier.SupplierProducts = new List<Product>();
            }

            var products = session.Products;
            foreach (var wcfProduct in wcfSupplier.SupplierProducts)
            {
                var productDb = products.FirstOrDefault(n => n.ProductId == wcfProduct.ProductId);
                if (productDb == null)
                {
                    session.Products.Add(new Product
                    {
                        Description = wcfProduct.Description,
                        Price = wcfProduct.Price,
                        ProductName = wcfProduct.ProductName,
                        Supplier = mySupplier
                    });
                }
                else
                {
                    productDb.Description = wcfProduct.Description;
                    productDb.Price = wcfProduct.Price;
                    productDb.ProductName = wcfProduct.ProductName;
                }
            }
        }

        internal static Event MapNewEvent(this WCFEvent wcfEvent, CrmDbContext session, ref Event myEvent)
        {
            if (wcfEvent == null)
            {
                return null;
            }

            myEvent.EventId = wcfEvent.EventId;
            myEvent.Description = wcfEvent.Description;
            myEvent.EventCreationDate = wcfEvent.EventCreationDate;
            myEvent.EventCustomer = wcfEvent.EventCustomer.MapCustomer(session);
            myEvent.EventExecutionDate = wcfEvent.EventExecutionDate;
            myEvent.EventTitle = wcfEvent.EventTitle;
            myEvent.EventType = wcfEvent.EventType;
            myEvent.Products = new List<Product>();
            if (!Enum.TryParse(wcfEvent.Status.ToString(), out EventStatus status))
            {
                status = EventStatus.Reported;
            }
            myEvent.Status = status;

            if (myEvent.Tasks == null)
            {
                myEvent.Tasks = new List<Task>();
            }

            var wcfTasks = wcfEvent.Tasks;
            var tasksToAdd = new List<Task>();
            if (wcfTasks != null && wcfTasks.Count > 0)
            {
                foreach (var task in wcfTasks)
                {
                    var myTask = session.Taks.FirstOrDefault(n => n.TaskId == task.TaskId);
                    if (myTask == null)
                    {
                        myTask = new Task
                        {
                            Finished = task.Finished,
                            TaskDescription = task.TaskDescription,
                            Title = task.Title,
                            TaskEvent = myEvent
                        };
                        tasksToAdd.Add(myTask);
                        myEvent.Tasks.Add(myTask);
                    }
                    else
                    {
                        myTask.Finished = task.Finished;
                        myTask.TaskDescription = task.TaskDescription;
                        myTask.Title = task.Title;
                        myTask.TaskEvent = myEvent;
                    }
                }

                if (tasksToAdd.Count > 0)
                {
                    session.Taks.AddRange(tasksToAdd);
                    //session.SaveChanges();
                }
            }

            var address = wcfEvent.EventAddress;
            if (address != null)
            {
                myEvent.EventAddress = session.Addresses.FirstOrDefault(n => n.AddressId == address.AddressId) ?? new Address();
                myEvent.EventAddress.AddressId = address.AddressId;
                myEvent.EventAddress.City = address.City;
                myEvent.EventAddress.Country = address.Country;
                myEvent.EventAddress.HomeExt = address.HomeExt;
                myEvent.EventAddress.Street = address.Street;
                myEvent.EventAddress.HomeNo = address.HomeNo;
                myEvent.EventAddress.ZipCode = address.ZipCode;
            }

            return myEvent;
        }

        internal static WCFEvent MapNewEvent(this Event dboEvent)
        {
            if (dboEvent == null)
            {
                return null;
            }

            var address = dboEvent.EventAddress ?? new Address();
            
            var products = dboEvent.Products.Select(product => new WCFProduct
            {
                ProductId = product.ProductId,
                Description = product.Description,
                Price = product.Price,
                ProductName = product.ProductName,
                Supplier = new WCFSupplier
                {
                    Address = new WCFAddress
                    {
                        City = product.Supplier.Address.City,
                        ZipCode = product.Supplier.Address.ZipCode,
                        Street = product.Supplier.Address.Street,
                        HomeNo = product.Supplier.Address.HomeNo
                    }
                }
            });

            var newEvent = new WCFEvent
            {
                EventId = dboEvent.EventId,
                Description = dboEvent.Description,
                EventAddress = new WCFAddress
                {
                    AddressId = address.AddressId,
                    City = address.City,
                    Country = address.Country,
                    HomeExt = address.HomeExt,
                    HomeNo = address.HomeNo,
                    Street = address.Street,
                    ZipCode = address.ZipCode
                },
                EventCreationDate = dboEvent.EventCreationDate,
                EventCustomer = dboEvent.EventCustomer.MapCustomer(),
                EventExecutionDate = dboEvent.EventExecutionDate,
                EventTitle = dboEvent.EventTitle,
                EventType = dboEvent.EventType,
                Products = products.ToList(),
                Tasks = new List<WCFTask>()
            };
            if (!Enum.TryParse(dboEvent.Status.ToString(), out EventStatus status))
            {
                status = EventStatus.Reported;
            }
            newEvent.Status = status;

            var tasks = dboEvent.Tasks;
            if (tasks != null && tasks.Count > 0)
            {
                foreach (var myTask in tasks)
                {
                    newEvent.Tasks.Add(new WCFTask
                    {
                        TaskId = myTask.TaskId,
                        Finished = myTask.Finished,
                        TaskDescription = myTask.TaskDescription,
                        Title = myTask.Title
                    });
                }
            }

            return newEvent;
        }

        internal static WCFUser MapUser(this User user)
        {
            if (user == null)
            {
                return null;
            }

            if (user.UserAddress == null)
            {
                user.UserAddress = new Address();
            }
            return new WCFUser
            {
                WcfUserId = user.UserId,
                CreationDate = user.CreationDate,
                DateOfBirth = user.DateOfBirth,
                Description = user.Description,
                Email = user.Email,
                FirstName = user.FirstName,
                Login = user.Login,
                PhoneNo = user.PhoneNo,
                Profession = user.Profession,
                Role = user.Role,
                SecondName = user.SecondName,
                Password = user.Password,
                UserAddress = new WCFAddress
                {
                    AddressId = user.UserAddress.AddressId,
                    City = user.UserAddress.City,
                    Country = user.UserAddress.Country,
                    HomeExt = user.UserAddress.HomeExt,
                    HomeNo = user.UserAddress.HomeNo,
                    Street = user.UserAddress.Street,
                    ZipCode = user.UserAddress.ZipCode,
                }
            };
        }

        internal static User MapUser(this WCFUser wcfUser, CrmDbContext session)
        {
            if (wcfUser == null)
            {
                return null;
            }

            var wcfAddress = wcfUser.UserAddress ?? new WCFAddress();
            var dbAddress = session.Addresses.FirstOrDefault(n => n.AddressId == wcfAddress.AddressId) ?? new Address();
            dbAddress.HomeExt = wcfAddress.HomeExt;
            dbAddress.HomeNo = wcfAddress.HomeNo;
            dbAddress.ZipCode = wcfAddress.ZipCode;
            dbAddress.City = wcfAddress.City;
            dbAddress.Country = wcfAddress.Country;
            dbAddress.Street = wcfAddress.Street;

            var user = session.Users.FirstOrDefault(n => n.UserId == wcfUser.WcfUserId) ?? new User();
            user.CreationDate = wcfUser.CreationDate;
            user.DateOfBirth = wcfUser.DateOfBirth;
            user.Description = wcfUser.Description;
            user.Email = wcfUser.Email;
            user.FirstName = wcfUser.FirstName;
            user.Login = wcfUser.Login;
            user.PhoneNo = wcfUser.PhoneNo;
            user.Profession = wcfUser.Profession;
            user.Role = wcfUser.Role;
            user.SecondName = wcfUser.SecondName;
            user.UserAddress = dbAddress;
            user.Password = wcfUser.Password;
            
            return user;
        }
    }
}