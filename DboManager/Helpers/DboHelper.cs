﻿using System;
using System.Collections.Generic;
using System.Linq;
using DboManager.DboModels;
using DboManager.Managers;
using DboManager.Models;
using DboManager.ResponseModels;

namespace DboManager.Helpers
{
    public class DboHelper
    {
        public GetAllSuppliersResponse GetAllSuppliers()
        {
            try
            {
                using (var session = new CrmDbContext())
                {

                    var suppliers = session.Suppliers.ToList();
                    var wcfSuppliers = new List<WCFSupplier>();
                    foreach (var supplier in suppliers)
                    {
                        var address = supplier.Address ?? new Address();
                        var products = supplier.SupplierProducts ?? new List<Product>();
                        var contactPerson = supplier.ContactPerson ?? new ContactPerson();

                        var wcfSupplier = new WCFSupplier
                        {
                            SupplierCompanyName = supplier.SupplierCompanyName,
                            SupplierId = supplier.SupplierId,
                            Details = supplier.Details,
                            CreationDate = supplier.CreationDate,
                            Address = new WCFAddress
                            {
                                AddressId = address.AddressId,
                                City = address.City,
                                Country = address.Country,
                                HomeExt = address.HomeExt ?? string.Empty,
                                Street = address.Street,
                                HomeNo = address.HomeNo,
                                ZipCode = address.ZipCode
                            },
                            ContactPerson = new WCFContactPerson
                            {
                                ContactPersonId = contactPerson.ContactPersonId,
                                Name = contactPerson.Name,
                                Phone = contactPerson.Phone,
                                Profession = contactPerson.Profession,
                                SecondName = contactPerson.SecondName
                            }
                        };

                        var wcfProducts = products.Select(product => new WCFProduct
                        {
                            Description = product.Description,
                            Price = product.Price,
                            ProductId = product.ProductId,
                            ProductName = product.ProductName,
                            Supplier = wcfSupplier
                        }).ToList();

                        wcfSupplier.SupplierProducts = wcfProducts;
                        wcfSuppliers.Add(wcfSupplier);
                    }

                    return new GetAllSuppliersResponse { WCFSuppliers = wcfSuppliers };
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
                return new GetAllSuppliersResponse();
            }
        }

        public List<WCFCustomer> GetAllCustomers()
        {
            try
            {
                using (var session = new CrmDbContext())
                {
                    var customers = session.Customers.ToList();
                    return customers.Select(MappingHelper.MapCustomer).ToList();
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
                return new List<WCFCustomer>();
            }
        }

        public List<WCFEvent> GetAllEvents()
        {
            try
            {
                using (var session = new CrmDbContext())
                {
                    var events = session.Events.ToList();
                    return events.Select(MappingHelper.MapNewEvent).ToList();
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
                return new List<WCFEvent>();
            }
        }

        public AddDataResponse AddEvent(WCFEvent wcfEvent)
        {
            try
            {   
                using (var session = new CrmDbContext())
                {
                    var dboProducts = session.Products;
                    var newEvent = session.Events.FirstOrDefault(n => n.EventId == wcfEvent.EventId);
                    if (newEvent != null)
                    {
                        session.Events.Remove(newEvent);
                    }

                    newEvent = new Event();
                    
                    wcfEvent.MapNewEvent(session, ref newEvent);
                    foreach (var product in wcfEvent.Products)
                    {
                        newEvent.Products.Add(dboProducts.FirstOrDefault(n => n.ProductId == product.ProductId));
                    }

                    session.Events.Add(newEvent);
                    session.SaveChanges();
                    return new AddDataResponse(true, $"Event: '{wcfEvent.EventTitle}' successfully added.");
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
            }
            var message = wcfEvent != null ? $"Event: '{wcfEvent.EventTitle}' not added." : "Provided event is null.";
            return new AddDataResponse(false, $"{message} Please check logs for more information");
        }

        public void DeleteEvent(int eventId)
        {
            try
            {
                using (var session = new CrmDbContext())
                {
                    var myEvent = session.Events.FirstOrDefault(n => n.EventId == eventId);
                    if (myEvent != null)
                    {
                        session.Taks.RemoveRange(myEvent.Tasks);
                        if (myEvent.EventAddress != null)
                        {
                            var address = session.Addresses.FirstOrDefault(n => n.AddressId == myEvent.EventAddress.AddressId);
                            if (address != null)
                            {
                                session.Addresses.Remove(address);
                            }
                        }
                        
                        session.Events.Remove(myEvent);
                    }

                    session.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
            }
        }

        public List<WCFMeeting> GetAllMeetings()
        {
            try
            {
                using (var session = new CrmDbContext())
                {
                    var meetings = session.Meetings.ToList();
                    return meetings.Select(meeting => new WCFMeeting
                    {
                        MeetingId = meeting.MeetingId,
                        Description = meeting.Description,
                        Date = meeting.Date,
                        Title = meeting.Title
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
                return new List<WCFMeeting>();
            }
        } 

        public AddDataResponse AddMeeting(WCFMeeting wcfMeeting)
        {
            try
            {
                using (var session = new CrmDbContext())
                {
                    var meeting = session.Meetings.FirstOrDefault(n => n.MeetingId == wcfMeeting.MeetingId);
                    if (meeting == null)
                    {
                        session.Meetings.Add(new Meeting
                        {
                            Date = wcfMeeting.Date,
                            Description = wcfMeeting.Description,
                            Title = wcfMeeting.Title
                        });
                    }
                    else
                    {
                        meeting.Description = wcfMeeting.Description;
                        meeting.Date = wcfMeeting.Date;
                        meeting.Title = wcfMeeting.Title;
                    }

                    session.SaveChanges();
                    return new AddDataResponse(true, $"Customer: '{wcfMeeting.Title}' successfully added.");
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
                var message = wcfMeeting != null ? $"Meeting: '{wcfMeeting.Title}' not added." : "Provided meeting is null.";
                return new AddDataResponse(false, $"{message} Please check logs for more information");
            }
        }

        public DeleteDataResponse RemoveMeeting(int meetingId)
        {
            using (var session = new CrmDbContext())
            {
                var meeting = session.Meetings.FirstOrDefault(n => n.MeetingId == meetingId);
                if (meeting != null)
                {
                    session.Meetings.Remove(meeting);
                    session.SaveChanges();
                    return new DeleteDataResponse { Success = true, Message = $"Meeting: '{meeting.Title}' successfully deleted." };
                }

                return new DeleteDataResponse { Success = false, Message = $"Cannot delete meeting with id: '{meetingId}'. Please check logs for more information" };
            }
        }

        public AddDataResponse AddCustomer(WCFCustomer customer)
        {
            try
            {
                using (var session = new CrmDbContext())
                {
                    if (!session.Customers.Any(n => n.CompanyName.Equals(customer.CompanyName)))
                    {
                        session.Customers.Add(customer.MapCustomer(session));
                        session.SaveChanges();
                        return new AddDataResponse(true, $"Customer: '{customer.CompanyName}' successfully added.");
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
            }
            var message = customer != null ? $"Supplier: '{customer.CompanyName}' not added." : "Provided customer is null.";
            return new AddDataResponse(false, $"{message} Please check logs for more information");
        }

        public AddDataResponse AddSupplier(WCFSupplier supplier)
        {
            try
            {
                using (var session = new CrmDbContext())
                {
                    if (!session.Suppliers.Any(n=>n.SupplierCompanyName.Equals(supplier.SupplierCompanyName)))
                    {
                        var mySupplier = new Supplier();
                        supplier.MapSupplier(session, ref mySupplier);

                        session.Suppliers.Add(mySupplier);
                        session.SaveChanges();
                        return new AddDataResponse(true, $"Supplier: '{supplier.SupplierCompanyName}' successfully added.");
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
            }
            var message = supplier != null ? $"Supplier: '{supplier.SupplierCompanyName}' not added." : "Provided supplier is null.";
            return new AddDataResponse(false, $"{message} Please check logs for more information");
        }

        public DeleteDataResponse DeleteSupplier(int supplierId)
        {
            using (var session = new CrmDbContext())
            {
                var supplier = session.Suppliers.FirstOrDefault(n => n.SupplierId == supplierId);
                if (supplier != null)
                {
                    session.Products.RemoveRange(supplier.SupplierProducts);
                    session.Suppliers.Remove(supplier);
                    session.SaveChanges();
                    return new DeleteDataResponse { Success = true, Message = $"Supplier id: '{supplier.SupplierId}', name: '{supplier.SupplierCompanyName}' successfully deleted."};
                }

                return new DeleteDataResponse { Success = false, Message = $"Cannot delete supplier with id: '{supplierId}'. Please check logs for more information"};
            }
        }

        public AddDataResponse UpdateSupplier(WCFSupplier wcfSupplier)
        {
            using (var session = new CrmDbContext())
            {
                var supplier = session.Suppliers.FirstOrDefault(n => n.SupplierId == wcfSupplier.SupplierId);
                if (supplier != null)
                {
                    wcfSupplier.MapSupplier(session, ref supplier);
                    session.SaveChanges();
                    return new AddDataResponse(true, $"Supplier id: '{supplier.SupplierId}', name: '{supplier.SupplierCompanyName}' successfully deleted.");
                }

                return new AddDataResponse (false, $"Cannot delete supplier with id: '{wcfSupplier.SupplierId}'. Please check logs for more information");
            }
        }

        public DeleteDataResponse DeleteCustomer(int customerId)
        {
            using (var session = new CrmDbContext())
            {
                var customer = session.Customers.FirstOrDefault(n => n.CustomerId == customerId);
                if (customer != null)
                {
                    session.Customers.Remove(customer);
                    session.SaveChanges();
                    return new DeleteDataResponse { Success = true, Message = $"Customer id: '{customer.CustomerId}', name: '{customer.CompanyName}' successfully deleted." };
                }

                return new DeleteDataResponse { Success = false, Message = $"Cannot delete supplier with id: '{customerId}'. Please check logs for more information" };
            }
        }
    }
}