﻿using System;
using System.Linq;
using DboManager.DboModels;
using DboManager.Managers;
using DboManager.Models;
using DboManager.ResponseModels;

namespace DboManager.Helpers
{
    public class LoginHelper
    {
        private const string WrongLoginOfPassword = "Wrong login or password";
        public LoginResponse LogIn(string login, string password)
        {
            try
            {
                using (var session = new CrmDbContext())
                {
                    LogManager.Log($"Checking login: '{login}'");
                    LogManager.Log("Before call database");
                    var user = session.Users.FirstOrDefault(n => n.Login == login && n.Password == password);
                    var logged = user != null;
                    var msg = logged ? $"Welcome {user.FirstName}" : WrongLoginOfPassword;
                    LogManager.Log($"Message from WCF: {msg}");
                   
                    return new LoginResponse(user != null, msg, user.MapUser());
                }
            }
            catch (Exception e)
            {
                LogManager.Log($"Unexpected error occured while login process: {e}");
                return new LoginResponse(false, "Unexpected error.", null);
            }
            
        }

        public LoginResponse AddUser(WCFUser wcfUser)
        {
            try
            {
                using (var session = new CrmDbContext())
                {
                    var user = wcfUser.MapUser(session);
                    if (user != null)
                    {
                        var current = session.Users.FirstOrDefault(n => n.UserId == user.UserId);
                        if (current != null)
                        {
                            current.Description = user.Description;
                            current.CreationDate = user.CreationDate;
                            current.DateOfBirth = user.DateOfBirth;
                            current.Email = user.Email;
                            current.FirstName = user.FirstName;
                            current.Login = user.Login;
                            current.Password = user.Password;
                            current.PhoneNo = user.PhoneNo;
                            current.Profession = user.Profession;
                            current.Role = user.Role;
                            current.SecondName = user.SecondName;
                        }
                        else
                        {
                            session.Users.Add(user);
                        }
                        
                        session.SaveChanges();
                        return new LoginResponse(true, $"User {user.Login} successfully added.", user.MapUser());
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(ex.Message, LogLevel.Error);
                return new LoginResponse(true, $"Unexpected error occured. Please check logs for more information.", null);
            }

            return new LoginResponse(true, $"User {wcfUser?.Login} not added.", null);
        }
    }
}