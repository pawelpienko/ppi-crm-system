IF NOT EXISTS (SELECT 1 FROM SYS.PROCEDURES WHERE NAME = N'PR_InsertNewAddress_01')
	BEGIN
		EXEC('CREATE PROCEDURE PR_InsertNewAddress_01
		(
			@HomeNo varchar(8),
			@HomeExt varchar(8),
			@Street varchar(50),
			@City nvarchar(40),
			@Country nvarchar(40),
			@Details nvarchar(250),
			@Result int OUTPUT
		) 
		AS
			BEGIN
				IF NOT EXISTS (SELECT * FROM TB_ADDRESS 
                   WHERE HOME_NO = @HomeNo
                   AND HOME_EXT = @HomeExt
				   AND STREET = @Street
				   AND CITY = @City
				   AND COUNTRY = @Country
				   AND DETAILS = @Details)
				BEGIN
				INSERT INTO TB_ADDRESS (HOME_NO, HOME_EXT, STREET, CITY, COUNTRY, DETAILS)
				VALUES (@HomeNo, @HomeExt, @Street, @City, @Country, @Details);
				
				SELECT @Result = @@ROWCOUNT;
			END
		END');
	END;
	
IF NOT EXISTS (SELECT 1 FROM SYS.PROCEDURES WHERE NAME = N'PR_InsertNewPerson_01')
	BEGIN
		EXEC('CREATE PROCEDURE PR_InsertNewPerson_01
		(
			@Name varchar(50),
			@SecondName varchar(50),
			@Profession varchar(50),
			@Salary decimal(7,2),
			@AddressId nvarchar(40),
			@Result int OUTPUT
		) 
		AS
			BEGIN
				IF NOT EXISTS (SELECT * FROM TB_PERSONS 
                   WHERE NAME = @Name
                   AND SECOND_NAME = @SecondName
				   AND PROFESSION = @Profession
				   AND SALARY = @Salary
				   AND ADDRESS_ID = @AddressId)
				BEGIN
				INSERT INTO TB_PERSONS (NAME, SECOND_NAME, PROFESSION, SALARY, ADDRESS_ID)
				VALUES (@Name, @SecondName, @Profession, @Salary, @AddressId);
				
				SELECT @Result = @@ROWCOUNT;
			END
		END');
	END;
	
IF NOT EXISTS (SELECT 1 FROM SYS.PROCEDURES WHERE NAME = N'PR_InsertNewUser_01')
	BEGIN
		EXEC('CREATE PROCEDURE PR_InsertNewUser_01
		(
			@Login varchar(50),
			@Password varchar(50),
			@CreationTime datetime,
			@ImagePath varchar(100),
			@Description nvarchar(250),
			@PersonId nvarchar(8),
			@Result int OUTPUT
		) 
		AS
			BEGIN
				IF NOT EXISTS (SELECT * FROM TB_USERS 
                   WHERE LOGIN = @Login
                   AND PASSWORD = @Password)
				BEGIN
				INSERT INTO TB_USERS (LOGIN, PASSWORD, CREATION_TIME, IMAGE_PATH, DESCRIPTION, PERSON_ID)
				VALUES (@Login, @Password, @CreationTime, @ImagePath, @Description, @PersonId);
				
				SELECT @Result = @@ROWCOUNT;
			END
		END');
	END;
	
IF NOT EXISTS (SELECT 1 FROM SYS.PROCEDURES WHERE NAME = N'PR_InsertNewClient_01')
	BEGIN
		EXEC('CREATE PROCEDURE PR_InsertNewClient_01
		(
			@CompanyName varchar(50),
			@Details varchar(250),
			@AddressId varchar(8),
			@ContactPersonId varchar(8),
			@Result int OUTPUT
		) 
		AS
			BEGIN
				IF NOT EXISTS (SELECT * FROM TB_CLIENTS 
                   WHERE COMPANY_NAME = @CompanyName)
				BEGIN
				INSERT INTO TB_CLIENTS (COMPANY_NAME, DETAILS, ADDRESS_ID, CONTACT_PERSON_ID)
				VALUES (@CompanyName, @Details, @AddressId, @ContactPersonId);
				SELECT @Result = @@ROWCOUNT;
			END
		END');
	END;