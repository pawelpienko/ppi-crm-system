﻿using System;
using System.Drawing;
using iTextSharp.text;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;

namespace CRMSystem.PdfDocumentManager
{
    internal static class PdfGeneratorHelper
    {
        internal static void AddElement(this Document document, Image image, float scalePercent = 0, Alignment alightment = Alignment.Center)
        {
            if (image == null || document == null)
            {
                return;
            }
            image.ScalePercent(scalePercent);
            image.Alignment = (int)alightment;
            document.Add(image);
        }

        private static readonly Font Font11 = FontFactory.GetFont("NewJune", 10, BaseColor.BLACK);

        internal static void AddElement(this Document document, string text, Alignment alignment = Alignment.Center, int fontSize = 16, Font font = null, BaseColor color = null)
        {
            if (document == null)
            {
                return;
            }

            var providedFont = (font ?? Font11);
            providedFont.Size = fontSize;
            var paragraph = new Paragraph(text, providedFont)
            {
                Alignment = (int)alignment
            };

            document.Add(paragraph);
        }

        internal static void AddNormalText(this Document document, string text, Alignment alignment = Alignment.Left)
        {
            if (document == null)
            {
                return;
            }

            Font11.Size = 9;
            var paragraph = new Paragraph(text, Font11)
            {
                Alignment = (int)alignment
            };

            document.Add(paragraph);
        }

        internal static void AppendEmptyLine(this Document document)
        {
            document?.Add(new Paragraph("\n"));
        }
    }

    internal enum Alignment
    {
        Left = 0,
        Center = 1,
        Right = 2
    }
}