﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CRMSystem.Helpers;
using CRMSystem.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace CRMSystem.PdfDocumentManager
{
    public class PdfGenerator
    {
        private const string ProductsPdfFileName = "CRM_PRODUCTS";
      
        public void CreateProductsPdf(Event myEvent, string path)
        {
            var doc = new Document(PageSize.A4, 25, 25, 43, 35);
            PdfWriter.GetInstance(doc, new FileStream($@"{path}\\{ProductsPdfFileName}_{DateTime.Now.Ticks}.pdf", FileMode.Create));
            doc.Open();
            doc.AddElement(GetLogo(), 24f, Alignment.Right);
            doc.AppendEmptyLine();
            doc.AddElement($"Event: {myEvent.EventTitle}", Alignment.Left, 12);
            doc.AddElement($"Description:{myEvent.Description}", Alignment.Left, 12);
            doc.AddElement($"Execution date: {myEvent.EventExecutionDate:dd MMMM yyyy}", Alignment.Left, 12);
            doc.AppendEmptyLine();
            doc.AddElement("Product list");

            var table = new PdfPTable(5);
            table.SpacingBefore = 20;
            
            var arialUniCode = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIALUNI.TTF");
            var baseFont = BaseFont.CreateFont(arialUniCode, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            var fontBold = new Font(baseFont, 12, Font.BOLD);
            
            table.AddCell(new PdfPCell(new Phrase("Id:", fontBold)));
            table.AddCell(new PdfPCell(new Phrase("Product:", fontBold)));
            table.AddCell(new PdfPCell(new Phrase("Description:", fontBold)));
            table.AddCell(new PdfPCell(new Phrase("Price:", fontBold)));
            table.AddCell(new PdfPCell(new Phrase("Supplier address:", fontBold)));

            var normalFont = new Font(baseFont, 11, Font.NORMAL);
            var products = myEvent.Products;
            for (var i = 0; i < products.Count; i++)
            {
                table.AddCell(new PdfPCell(new Phrase((i + 1).ToString(), normalFont)));
                table.AddCell(new PdfPCell(new Phrase(products[i].ProductName, normalFont)));
                table.AddCell(new PdfPCell(new Phrase(products[i].Description, normalFont)));
                table.AddCell(new PdfPCell(new Phrase($"{products[i].Price:0.00} zł", normalFont)));
                table.AddCell(new PdfPCell(new Phrase(products[i].ProductSupplier.Address.GetAddressString(), normalFont)));
            }

            doc.Add(table);

            doc.AddElement(GetLogo(), 15f, Alignment.Left);

            doc.Close();
        }

        private static Image GetLogo()
        {
            try
            {
                return Image.GetInstance($"{GetProjectPath()}\\CRMSystem\\Images\\PdfDocumentMembers\\Logo.png");
            }
            catch (Exception ex)
            {
                //log info
                return null;
            }
        }

        private static string GetProjectPath()
        {
            var uri = new Uri(Assembly.GetExecutingAssembly().GetName().CodeBase);
            var loadPath = Path.GetDirectoryName(uri.LocalPath);
            var directory = new DirectoryInfo(loadPath);

            while (directory != null && !directory.GetFiles("*.sln").Any())
            {
                directory = directory.Parent;
            }
            return directory?.FullName;
        }
    }
}
