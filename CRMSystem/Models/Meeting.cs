﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;

namespace CRMSystem.Models
{
    public class Meeting
    {
        public int MeetingId { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string TimeString { get; set; }
        public string Description { get; set; }
        public Meeting()
        {
            Date = DateTime.Now;
        }

        public Meeting(int id, string title, DateTime date, string description)
        {
            MeetingId = id;
            Title = title;
            Date = date;
            Description = description;
            TimeString = Date.ToString("hh:mm");
        }
    }
}
