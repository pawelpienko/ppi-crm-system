﻿using System;

namespace CRMSystem.Models
{
    [Serializable]
    public class ContactPerson
    {
        public int ContactPersonId { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string Phone { get; set; }
        public Professions Profession { get; set; }
    }
}
