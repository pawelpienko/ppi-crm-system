﻿using System;

namespace CRMSystem.Models
{
    [Serializable]
    public class Customer
    {
        public int CustomerId { get; set; }
        public string CompanyName { get; set; }
        public Address Address { get; set; }
        public ContactPerson ContactPerson { get; set; }
        public string Details { get; set; }
        public DateTime CreationDate { get; set; }

        public Customer()
        {
            Address = new Address();
            ContactPerson = new ContactPerson();
            CreationDate = DateTime.Now;
        }

        public Customer(int id, string companyName, string contactPersonName, string city)
        {
            CustomerId = id;
            CompanyName = companyName;
            Details = "Company details";
            ContactPerson = new ContactPerson
            {
                Name = contactPersonName,
                Profession = Professions.Manager,
                Phone = "998887776",
                SecondName = "Second name"
            };
            Address = new Address
            {
                City = city,
                Country = "Poland",
                HomeNo = "2",
                HomeExt = "A",
                Street = "Szarych szeregów",
                ZipCode = "66-016"
            };
            CreationDate = DateTime.Now;
        }
    }
}
