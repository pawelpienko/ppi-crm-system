﻿using System;

namespace CRMSystem.Models
{
    [Serializable]
    public class Task
    {
        public int TaskId { get; set; }
        public string Title { get; set; }
        public string TaskDescription { get; set; }
        public bool Finished { get; set; }

        public Task(string title, string description = "", bool finished = false)
        {
            Title = title;
            TaskDescription = description;
            Finished = finished;
        }

        public Task()
        {
            
        }
    }
}
