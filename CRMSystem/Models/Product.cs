﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMSystem.Models
{
    [Serializable]
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public Supplier ProductSupplier { get; set; }
        public string NamePriceFormatted { get; set; }
        public List<Event> Events { get; set; }

        public Product()
        {
        }

        public Product(string name, decimal price)
        {
            ProductName = name;
            Price = price;
            NamePriceFormatted = $"{ProductName} ({Price.ToString("0.00", CultureInfo.InvariantCulture)} zł)";
        }
    }
}
