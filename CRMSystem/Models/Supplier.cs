﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CRMSystem.Models
{
    [Serializable]
    public class Supplier
    {
        public int SupplierId { get; set; }
        public string SupplierCompanyName { get; set; }
        public Address Address { get; set; }
        public ContactPerson ContactPerson { get; set; }
        public string Details { get; set; }
        public ObservableCollection<Product> SupplierProducts { get; set; }
        public DateTime CreationDate { get; set; }

        public Supplier()
        {
            Address = new Address();
            ContactPerson = new ContactPerson();
            SupplierProducts = new ObservableCollection<Product>();
            CreationDate = DateTime.Now;
        }
    }
}
