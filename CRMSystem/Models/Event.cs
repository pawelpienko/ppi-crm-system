﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;

namespace CRMSystem.Models
{
    [Serializable]
    public class Event
    {
        public int EventId { get; set; }
        public string EventTitle { get; set; }
        public DateTime EventExecutionDate { get; set; }
        public DateTime EventCreationDate { get; set; }
        public ObservableCollection<Task> Tasks { get; set; }
        public EventStatus Status { get; set; }
        public string Description { get; set; }
        public string ExecutionDateWithDesc { get; set; }
        public EventType EventType { get; set; }
        public Address EventAddress { get; set; }
        public Customer EventCustomer { get; set; }
        public ObservableCollection<Product> Products { get; set; }

        public Event()
        {
            Tasks = new ObservableCollection<Task>();
            Products = new ObservableCollection<Product>();
            EventAddress = new Address();
        }

        public Event(int id, string title, DateTime executionDate, string description, EventType eventType, Address eventAddress, Customer customer, ICollection<Task> tasks = null, EventStatus status = EventStatus.Reported)
        {
            EventId = id;
            EventTitle = title;
            EventExecutionDate = executionDate;
            EventCreationDate = DateTime.Now;
            Tasks = new ObservableCollection<Task>();
            if (tasks != null)
            {
                foreach (var task in tasks)
                {
                    Tasks.Add(task);
                }
            }

            EventType = eventType;
            Status = status;
            Description = description;
            ExecutionDateWithDesc = $"Execution date: {executionDate.ToString("dddd, dd MM yyyy", CultureInfo.InvariantCulture)}";
            EventCustomer = customer ?? new Customer();
            EventAddress = eventAddress ?? new Address();
            Products = new ObservableCollection<Product>();
        }
    }

    [Serializable]
    public enum EventStatus
    {
        Reported,
        UnderAnalysis,
        InProgress,
        Done
    }

    [Serializable]
    public enum EventType
    {
        Picnic,
        Jubilee,
        Conference,
        Opening,
        ThemeParty,
        Integration,
        Others
    }
}
