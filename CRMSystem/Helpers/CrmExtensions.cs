﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMSystem.Models;

namespace CRMSystem.Helpers
{
    public static class CrmExtensions
    {
        internal static string GetStreetHomeNoExt(this Address address)
        {
            if (address == null)
            {
                return string.Empty;
            }
            var homeExt = address.HomeExt;
            var separator = string.Empty;
            if (!string.IsNullOrEmpty(homeExt))
            {
                separator = "/";
            }

            return $"ul. {address.Street} {address.HomeNo}{separator}{address.HomeExt}";
        }

        internal static string GetAddressString(this Address address)
        {
            return address == null ? string.Empty : $"{address.GetStreetHomeNoExt()} {address.ZipCode} {address.City}".Trim();
        }
    }
}
