﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using CRMSystem.DboManagerService;
using CRMSystem.Models;
using Professions = CRMSystem.Models.Professions;

namespace CRMSystem.Helpers
{
    public static class MappingHelper
    {
        internal static WCFUser MapUser(this User user)
        {
            if (user == null)
            {
                return null;
            }

            if (!Enum.TryParse(user.Profession.ToString(), out DboManagerService.Professions prof))
            {
                prof = DboManagerService.Professions.Other;
            }
            if (!Enum.TryParse(user.Role.ToString(), out DboManagerService.UserRoles role))
            {
                role = DboManagerService.UserRoles.RegularUser;
            }

            return new WCFUser
            {
                WcfUserId = user.UserId,
                CreationDate = user.CreationDate,
                DateOfBirth = user.DateOfBirth,
                Description = user.Description,
                Email = user.Email,
                FirstName = user.FirstName,
                Login = user.Login,
                Password = user.Password,
                PhoneNo = user.PhoneNo,
                Profession = prof,
                Role = role,
                SecondName = user.SecondName,
                UserAddress = new WCFAddress
                {
                    AddressId = user.UserAddress.AddressId,
                    City = user.UserAddress.City,
                    Country = user.UserAddress.Country,
                    HomeExt = user.UserAddress.HomeExt,
                    HomeNo = user.UserAddress.HomeNo,
                    Street = user.UserAddress.Street,
                    ZipCode = user.UserAddress.ZipCode
                }
            };
        }

        internal static User MapUser(this WCFUser wcfUser)
        {
            if (wcfUser == null)
            {
                return null;
            }

            if (!Enum.TryParse(wcfUser.Profession.ToString(), out Professions prof))
            {
                prof = Professions.Other;
            }
            if (!Enum.TryParse(wcfUser.Role.ToString(), out Models.UserRoles role))
            {
                role = Models.UserRoles.RegularUser;
            }

            return new User
            {
                UserId = wcfUser.WcfUserId,
                CreationDate = wcfUser.CreationDate,
                DateOfBirth = wcfUser.DateOfBirth,
                Description = wcfUser.Description,
                Email = wcfUser.Email,
                FirstName = wcfUser.FirstName,
                Login = wcfUser.Login,
                PhoneNo = wcfUser.PhoneNo,
                Profession = prof,
                Password = wcfUser.Password,
                Role = role,
                UserAddress = new Address
                {
                    AddressId = wcfUser.UserAddress.AddressId,
                    City = wcfUser.UserAddress.City,
                    Country = wcfUser.UserAddress.Country,
                    HomeExt = wcfUser.UserAddress.HomeExt,
                    HomeNo = wcfUser.UserAddress.HomeNo,
                    Street = wcfUser.UserAddress.Street,
                    ZipCode = wcfUser.UserAddress.ZipCode
                }
            };
        }

        internal static Customer MapCustomer(this WCFCustomer wcfCustomer)
        {
            if (wcfCustomer == null)
            {
                return null;
            }

            var address = wcfCustomer.Address;
            var contactPerson = wcfCustomer.ContactPerson;
            if (!Enum.TryParse(contactPerson.Profession.ToString(), out Professions prof))
            {
                prof = Professions.Other;
            }
            return new Customer
            {
                CustomerId = wcfCustomer.CustomerId,
                CompanyName = wcfCustomer.CompanyName,
                Address = new Address
                {
                    AddressId = address.AddressId,
                    City = address.City,
                    Country = address.Country,
                    HomeExt = address.HomeExt,
                    HomeNo = address.HomeNo,
                    Street = address.Street,
                    ZipCode = address.ZipCode
                },
                ContactPerson = new ContactPerson
                {
                    ContactPersonId = contactPerson.ContactPersonId,
                    Name = contactPerson.Name,
                    Phone = contactPerson.Phone,
                    Profession = prof,
                    SecondName = contactPerson.SecondName
                },
                CreationDate = wcfCustomer.CreationDate,
                Details = wcfCustomer.Details
            };
        }

        internal static WCFCustomer MapCustomer(this Customer customer)
        {
            if (customer == null)
            {
                return null;
            }

            var address = customer.Address;
            var contactPerson = customer.ContactPerson;
            if (!Enum.TryParse(contactPerson.Profession.ToString(), out DboManagerService.Professions prof))
            {
                prof = DboManagerService.Professions.Other;
            }
            return new WCFCustomer
            {
                CustomerId = customer.CustomerId,
                CompanyName = customer.CompanyName,
                Address = new WCFAddress
                {
                    AddressId = address.AddressId,
                    City = address.City,
                    Country = address.Country,
                    HomeExt = address.HomeExt,
                    HomeNo = address.HomeNo,
                    Street = address.Street,
                    ZipCode = address.ZipCode
                },
                ContactPerson = new WCFContactPerson
                {
                    ContactPersonId = contactPerson.ContactPersonId,
                    Name = contactPerson.Name,
                    Phone = contactPerson.Phone,
                    Profession = prof,
                    SecondName = contactPerson.SecondName
                },
                CreationDate = customer.CreationDate,
                Details = customer.Details
            };
        }

        internal static Supplier MapSupplier(this WCFSupplier wcfSupplier)
        {
            var address = wcfSupplier.Address ?? new WCFAddress();
            var contactPerson = wcfSupplier.ContactPerson ?? new WCFContactPerson();
            if (!Enum.TryParse(contactPerson.Profession.ToString(), out Professions prof))
            {
                prof = Professions.Other;
            }

            var supplier = new Supplier
            {
                SupplierCompanyName = wcfSupplier.SupplierCompanyName,
                SupplierId = wcfSupplier.SupplierId,
                Details = wcfSupplier.Details,
                CreationDate = wcfSupplier.CreationDate,
                Address = new Address
                {
                    AddressId = address.AddressId,
                    City = address.City,
                    Country = address.Country,
                    HomeExt = address.HomeExt ?? string.Empty,
                    Street = address.Street,
                    HomeNo = address.HomeNo,
                    ZipCode = address.ZipCode
                },
                ContactPerson = new ContactPerson
                {
                    ContactPersonId = contactPerson.ContactPersonId,
                    Name = contactPerson.Name,
                    Phone = contactPerson.Phone,
                    Profession = prof,
                    SecondName = contactPerson.SecondName
                }
            };
            var supplierProducts = new ObservableCollection<Product>();
            var products = wcfSupplier.SupplierProducts;

            foreach (var product in products)
            {
                supplierProducts.Add(new Product
                {
                    Description = product.Description,
                    Price = product.Price,
                    ProductId = product.ProductId,
                    ProductName = product.ProductName,
                    ProductSupplier = supplier
                });
            }

            supplier.SupplierProducts = supplierProducts;
            return supplier;
        }

        internal static WCFSupplier MapSupplier(this Supplier crmSupplier)
        {
            if (crmSupplier == null)
            {
                return null;
            }

            var address = crmSupplier.Address ?? new Address();
            var contactPerson = crmSupplier.ContactPerson ?? new ContactPerson();
            if (!Enum.TryParse(contactPerson.Profession.ToString(), out DboManagerService.Professions prof))
            {
                prof = DboManagerService.Professions.Other;
            }

            var wcfSupplier = new WCFSupplier
            {
                SupplierCompanyName = crmSupplier.SupplierCompanyName,
                SupplierId = crmSupplier.SupplierId,
                Details = crmSupplier.Details,
                CreationDate = crmSupplier.CreationDate,
                Address = new WCFAddress
                {
                    AddressId = address.AddressId,
                    City = address.City,
                    Country = address.Country,
                    HomeExt = address.HomeExt ?? string.Empty,
                    Street = address.Street,
                    HomeNo = address.HomeNo,
                    ZipCode = address.ZipCode
                },
                ContactPerson = new WCFContactPerson
                {
                    ContactPersonId = contactPerson.ContactPersonId,
                    Name = contactPerson.Name,
                    Phone = contactPerson.Phone,
                    Profession = prof,
                    SecondName = contactPerson.SecondName
                }
            };

            var products = crmSupplier.SupplierProducts;
            var wcfProducts = products.Select(product => new WCFProduct
            {
                Description = product.Description,
                Price = product.Price,
                ProductId = product.ProductId,
                ProductName = product.ProductName,
                Supplier = wcfSupplier
            }).ToArray();

            wcfSupplier.SupplierProducts = wcfProducts;
            return wcfSupplier;
        }

        internal static WCFEvent MapEvent(this Event myEvent)
        {
            if (myEvent == null)
            {
                return null;
            }


            if (!Enum.TryParse(myEvent.EventType.ToString(), out DboManagerService.EventType type))
            {
                type = DboManagerService.EventType.Others;
            }
            if (!Enum.TryParse(myEvent.Status.ToString(), out DboManagerService.EventStatus status))
            {
                status = DboManagerService.EventStatus.Reported;
            }
            var newEvent = new WCFEvent
            {
                EventId = myEvent.EventId,
                Description = myEvent.Description,
                Status = status,
                EventCreationDate = myEvent.EventCreationDate,
                EventCustomer = myEvent.EventCustomer.MapCustomer(),
                EventExecutionDate = myEvent.EventExecutionDate,
                EventTitle = myEvent.EventTitle,
                EventType = type
            };

            var tasks = myEvent.Tasks;
            newEvent.Tasks = new WCFTask[tasks.Count];

            for (var i = 0; i < tasks.Count; i++)
            {
                newEvent.Tasks[i] = new WCFTask
                {
                    Finished = tasks[i].Finished,
                    TaskDescription = tasks[i].TaskDescription,
                    TaskId = tasks[i].TaskId,
                    Title = tasks[i].Title
                };

            }

            var address = myEvent.EventAddress;
            if (address != null)
            {
                newEvent.EventAddress = new WCFAddress
                {
                    AddressId = address.AddressId,
                    City = address.City,
                    Country = address.Country,
                    HomeExt = address.HomeExt,
                    HomeNo = address.HomeNo,
                    Street = address.Street,
                    ZipCode = address.ZipCode
                };
            }

            var products = myEvent.Products.ToList();
            var wcfProducts = products.Select(product => new WCFProduct
            {
                Description = product.Description,
                Price = product.Price,
                ProductId = product.ProductId
            }).ToList();

            newEvent.Products = wcfProducts.ToArray();
            return newEvent;
        }

        internal static Event MapEvent(this WCFEvent wcfEvent)
        {
            if (wcfEvent == null)
            {
                return null;
            }

            var address = wcfEvent.EventAddress ?? new WCFAddress();
            if (!Enum.TryParse(wcfEvent.EventType.ToString(), out Models.EventType type))
            {
                type = Models.EventType.Others;
            }
            if (!Enum.TryParse(wcfEvent.Status.ToString(), out Models.EventStatus status))
            {
                status = Models.EventStatus.Reported;
            }
            var newEvent = new Event
            {
                EventId = wcfEvent.EventId,
                Description = wcfEvent.Description,
                EventAddress = new Address
                {
                    AddressId = address.AddressId,
                    City = address.City,
                    Country = address.Country,
                    HomeExt = address.HomeExt,
                    HomeNo = address.HomeNo,
                    Street = address.Street,
                    ZipCode = address.ZipCode
                },
                Status = status,
                EventCreationDate = wcfEvent.EventCreationDate,
                EventCustomer = wcfEvent.EventCustomer.MapCustomer(),
                EventExecutionDate = wcfEvent.EventExecutionDate,
                EventTitle = wcfEvent.EventTitle,
                EventType = type,
                Tasks = new ObservableCollection<Models.Task>()
        };

            var eventProducts = new ObservableCollection<Product>();
            var products = wcfEvent.Products;
            if (wcfEvent.Tasks != null)
            {
                foreach (var myTask in wcfEvent.Tasks)
                {
                    newEvent.Tasks.Add(new Models.Task
                    {
                        Finished = myTask.Finished,
                        TaskDescription = myTask.TaskDescription,
                        TaskId = myTask.TaskId,
                        Title = myTask.Title
                    });
                }

            }

            foreach (var product in products)
            {
                var newProduct = new Product
                {
                    Description = product.Description,
                    Price = product.Price,
                    ProductId = product.ProductId,
                    ProductName = product.ProductName
                };
                if (product.Supplier != null)
                {
                    var newAddress = new Address();
                    if (product.Supplier.Address != null)
                    {
                        newAddress.City = product.Supplier.Address.City;
                        newAddress.Street = product.Supplier.Address.Street;
                        newAddress.HomeNo = product.Supplier.Address.HomeNo;
                        newAddress.ZipCode = product.Supplier.Address.ZipCode;
                    }

                    newProduct.ProductSupplier = new Supplier
                    {
                        Details = product.Supplier.Details,
                        Address = newAddress
                    };
                }
                eventProducts.Add(newProduct);

            }

            newEvent.Products = eventProducts;

            return newEvent;
        }
    }
}
