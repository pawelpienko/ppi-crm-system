﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRMSystem.DboManagerService;
using CRMSystem.Models;
using Address = CRMSystem.Models.Address;
using Product = CRMSystem.Models.Product;
using Professions = CRMSystem.Models.Professions;

namespace CRMSystem.Helpers
{

    public static class DBHelper
    {
        private static List<Product> _listOfProducts;
        private static List<Meeting> _listOfMeetings;
        private static List<Event> _listOfEvents;
        private static List<Customer> _listOfCustomers;
        private static List<Supplier> _listOfSuppliers;

        public static List<Product> ListOfProducts
        {
            get
            {
                var productsPerSupplier = ListOfSuppliers.Select(n => n.SupplierProducts).ToList();
                _listOfProducts = new List<Product>();
                foreach (var products in productsPerSupplier)
                {
                    _listOfProducts.AddRange(products);
                }

                return _listOfProducts;
            }
        }

        public static List<Meeting> ListOfMeetings
        {
            get { return GetAllMeetings(); }

        }

        public static List<Event> ListOfEvents
        {
            get
            {
                try
                {
                    var response = ServiceConnector.CurrentClient.GetAllEvents().ToList();
                    _listOfEvents = response.Select(MappingHelper.MapEvent).ToList();

                    return _listOfEvents;
                }
                catch (Exception e)
                {
                    return new List<Event>();
                }
            }
           
        }

        public static List<Customer> ListOfCustomers
        {
            get
            {
                List<WCFCustomer> response;
                try
                {
                    response = ServiceConnector.CurrentClient.GetAllCustomers().ToList();
                }
                catch (Exception e)
                {
                    return new List<Customer>();
                }

                PrepareCustomersFromResponse(response);
                return _listOfCustomers;
            }
        }

        public static List<Supplier> ListOfSuppliers
        {
            get
            {
                GetAllSuppliersResponse response;
                try
                {
                    response = ServiceConnector.CurrentClient.GetAllSuppliers();
                    PrepareSuppliersFromResponse(response?.WCFSuppliers.ToList());
                    return _listOfSuppliers;
                }
                catch (Exception e)
                {
                    return new List<Supplier>();
                }
            }
        }

        public static bool AddNewEvent(Event eventItem)
        {
            try
            {
                var response = ServiceConnector.CurrentClient.AddEvent(eventItem.MapEvent());
                return response.Success;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool AddCustomer(string companyName)
        {
            try
            {
                var response = ServiceConnector.CurrentClient.AddCustomer(new WCFCustomer
                {
                    CompanyName = companyName
                });

                return response.Success;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool AddSupplier(string companyName)
        {
            try
            {
                var response = ServiceConnector.CurrentClient.AddNewSupplier(new AddSupplierRequest
                {
                    Supplier = new WCFSupplier
                    {
                        SupplierCompanyName = companyName,
                        CreationDate = DateTime.Now,
                        SupplierProducts = new WCFProduct[0]
                    }
                });

                return response.Success;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool DeleteSupplier(int supplierId)
        {
            try
            {
                var response = ServiceConnector.CurrentClient.DeleteSupplier(supplierId);
                return response.Success;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static void DeleteEvent(int changedEventId)
        {
            ServiceConnector.CurrentClient.DeleteEvent(changedEventId);
        }

        public static void SaveUser(User user)
        {
            if (user != null)
            {
                ServiceConnector.CurrentClient.AddUser(user.MapUser());
            }
        }

        public static User LogIn(string login, string password, out string message, out bool success)
        {
            var response = ServiceConnector.CurrentClient.LogIn(login, password);
            success = response.Success;
            message = response.Message;
            return response.WCFUser.MapUser();
        }

        public static void DeleteMeeting(int meetingId)
        {
            ServiceConnector.CurrentClient.RemoveMeeting(meetingId);
        }

        public static void AddMeeting(Meeting meeting)
        {
            var wcfMeeting = new WCFMeeting
            {
                MeetingId = meeting.MeetingId,
                Title = meeting.Title,
                Date = meeting.Date,
                Description = meeting.Description
            };
            ServiceConnector.CurrentClient.AddMeeting(wcfMeeting);
        }

        public static List<Meeting> GetAllMeetings()
        {
            try
            {
                var meetings = ServiceConnector.CurrentClient.GetAllMeetings();
                return meetings.Select(meeting => new Meeting(meeting.MeetingId, meeting.Title, meeting.Date, meeting.Description)).ToList();
            }
            catch (Exception)
            {
                return new List<Meeting>();
            }
        }

        public static bool DeleteCustomer(int customerId)
        {
            try
            {
                var response = ServiceConnector.CurrentClient.DeleteCustomer(customerId);
                return response.Success;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static void UpdateListOfSuppliers(Supplier changedSupplier)
        {
            var updateResponse = ServiceConnector.CurrentClient.UpdateSupplier(changedSupplier.MapSupplier());
        }

        public static void UpdateListOfCustomers(Customer changedCustomer)
        {
            try
            {
                var deleteResponse = ServiceConnector.CurrentClient.DeleteCustomer(changedCustomer.CustomerId);
                var addResponse = ServiceConnector.CurrentClient.AddCustomer(changedCustomer.MapCustomer());
            }
            catch (Exception e)
            {
            }
        }

        public static void UpdateListOfEvents(Event changedEvent)
        {
            var updateResponse = ServiceConnector.CurrentClient.AddEvent(changedEvent.MapEvent());
        }

        private static void PrepareCustomersFromResponse(List<WCFCustomer> wcfCustomers)
        {
            _listOfCustomers = new List<Customer>();
            if (wcfCustomers == null || wcfCustomers.Count < 0)
            {
                return;
            }

            foreach (var customer in wcfCustomers)
            {
                _listOfCustomers.Add(customer.MapCustomer());
            }
        }

        private static void PrepareSuppliersFromResponse(List<WCFSupplier> suppliers)
        {
            _listOfSuppliers = new List<Supplier>();
            if (suppliers == null || suppliers.Count <= 0)
            {
                return;
            }

            foreach (var wcfSupplier in suppliers)
            {
                _listOfSuppliers.Add(wcfSupplier.MapSupplier());
            }
        }

        //public static User GetUserTest()
        //{
        //    return new User()
        //    {
        //        FirstName = "Paweł",
        //        SecondName = "Pieńko",
        //        UserAddress = new Address()
        //        {
        //            City = "Przewóz",
        //            Country = "Poland",
        //            HomeExt = "B",
        //            HomeNo = "3",
        //            Street = "Zielona",
        //            ZipCode = "68-132"
        //        },
        //        CreationDate = new DateTime(2019, 05, 02),
        //        DateOfBirth = new DateTime(1993, 05, 24),
        //        Profession = Professions.Programmer,
        //        Role = UserRoles.Administrator,
        //        PhoneNo = "736086599",
        //        Email = "pawelpienko11@gmail.com",
        //        Description = "Hello everyone, my name is Paweł and welcome in my CRM system!"

        //    };
        //}
    }
}
