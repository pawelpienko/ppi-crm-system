﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using CRMSystem.Models;
using CRMSystem.Properties;
using Tulpep.NotificationWindow;
using Timer = System.Timers.Timer;

namespace CRMSystem.Helpers
{
    internal class NotificationHelper
    {
        private const int PopupDelay = 5000;
        private const int SearchingInterval = 60000;
        private static List<int> _notifiedMeetingIds;
        internal void Initialize()
        {
            var timer = new Timer(SearchingInterval);
            _notifiedMeetingIds = new List<int>();
            timer.Elapsed += OnTimedEvent;
            timer.Start();
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            var nearestMeetings = GetNearestMeeting();
            foreach (var meeting in nearestMeetings)
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    var popup = new PopupNotifier
                    {
                        Image = Resources.TimerIcon,
                        TitleText = $"Soon: {meeting.Title}",
                        ContentText = meeting.TimeString
                    };
                    popup.Delay = PopupDelay;
                    popup.Popup();
                    _notifiedMeetingIds.Add(meeting.MeetingId);
                });
            }
        }

        private static IEnumerable<Meeting> GetNearestMeeting()
        {
            var currentDate = DateTime.Now;
            return DBHelper.ListOfMeetings.Where(n => n.Date.ToShortDateString() == currentDate.ToShortDateString() 
            && !_notifiedMeetingIds.Contains(n.MeetingId) 
            && n.Date.AddMinutes(15) >= currentDate).ToList();
        }
    }
}