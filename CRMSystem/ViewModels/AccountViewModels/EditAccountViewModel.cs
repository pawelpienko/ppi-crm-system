﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;

namespace CRMSystem.ViewModels
{
    public class EditAccountViewModel : BaseViewModel
    {
        private User _currentUser;
        private Address _currentUserAddress;

        public EditAccountViewModel(BaseViewModel view, User user) : base(view)
        {
            DiscardChangesCommand = new RelayCommand(DiscardEditedChanges);
            SaveChangesCommand = new RelayCommand(SaveEditedChanges);
            //_currentUser = InstancesHelper.Clone(CurrentUser);
            
            _currentUser = user ?? new User {UserAddress = new Address(), DateOfBirth = DateTime.Now};
            _currentUserAddress = _currentUser.UserAddress;
            var s = AdminModeEnabled;
        }

        private static readonly ObservableCollection<string> _roles = new ObservableCollection<string>
        {
            UserRoles.Administrator.ToString(),
            UserRoles.RegularUser.ToString()
        };

        public ObservableCollection<string> Roles
        {
            get { return _roles; }
            set { }
        }

        private string _selectedRole;
        public string SelectedRole
        {
            get { return _currentUser.Role.ToString(); }
            set
            {
                if (!Enum.TryParse(value, out UserRoles role))
                {
                    role = UserRoles.RegularUser;
                }
                _currentUser.Role = role;
            }
        }

        #region properties

        private string _login;

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                if (AdminModeEnabled)
                {
                    _currentUser.Login = _login;
                }
            }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                if (AdminModeEnabled)
                {
                    _currentUser.Password = _password;
                }
            }
        }

        public string Description
        {
            get
            {
                return _currentUser.Description;

            }
            set { _currentUser.Description = value; }
        }

        public string Country
        {
            get
            {
                return _currentUser.UserAddress.Country;

            }
            set { _currentUser.UserAddress.Country = value; }
        }

        public string FirstName
        {
            get
            {
                return _currentUser.FirstName;

            }
            set { _currentUser.FirstName = value; }
        }

        public string SecondName
        {
            get
            {
                return _currentUser.SecondName;

            }
            set { _currentUser.SecondName = value; }
        }

        public DateTime DateOfBirth
        {
            get { return _currentUser.DateOfBirth; }
            set
            {
                _currentUser.DateOfBirth = value;
            }
        }

        public string Profession
        {
            get { return _currentUser.Profession.ToString(); }
            set
            {
                if (Enum.TryParse(value, out Professions result))
                {
                    _currentUser.Profession = result;
                }
                else
                {
                    //TODO dodac allerting!//TODO dodac allerting!
                }

            }
        }

        public string HomeNo
        {
            get
            {
                return _currentUserAddress.HomeNo;

            }
            set { _currentUserAddress.HomeNo = value; }
        }

        public string HomeExt
        {
            get
            {
                return _currentUserAddress.HomeExt;

            }
            set { _currentUserAddress.HomeExt = value; }
        }

        public string Street
        {
            get
            {
                return _currentUserAddress.Street;

            }
            set { _currentUserAddress.Street = value; }
        }

        public string City
        {
            get
            {
                return _currentUserAddress.City;

            }
            set { _currentUserAddress.City = value; }
        }

        public string ZipCode
        {
            get
            {
                return _currentUserAddress.ZipCode;

            }
            set { _currentUserAddress.ZipCode = value; }
        }

        #endregion

        #region ICommandMembers and related methods

        public ICommand SaveChangesCommand { get; set; }
        public ICommand DiscardChangesCommand { get; set; }
        private void SaveEditedChanges()
        {
            CurrentUser = _currentUser;
            CurrentUser.CreationDate = _currentUser.CreationDate == DateTime.MinValue ? DateTime.Now : _currentUser.CreationDate;
            CurrentUser.DateOfBirth = _currentUser.DateOfBirth == DateTime.MinValue ? DateTime.Now : _currentUser.DateOfBirth;
            DBHelper.SaveUser(_currentUser);
            BaseView.SelectedViewModel = new AccountViewModel(BaseView);
        }

        private void DiscardEditedChanges()
        {
            BaseView.SelectedViewModel = new AccountViewModel(BaseView);
        }

        #endregion
    }
}
