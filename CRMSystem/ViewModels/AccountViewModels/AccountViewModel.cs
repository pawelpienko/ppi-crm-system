﻿using System;
using System.Globalization;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;
using CRMSystem.Properties;

namespace CRMSystem.ViewModels
{
    public class AccountViewModel : BaseViewModel
    {
        private const string DEFAULT_MALE_ACCOUNT_IMAGE_SOURCE = "/CRMSystem;component/Images/DefaultAccountMaleImg.png";
        private const string DEFAULT_FEMALE_ACCOUNT_IMAGE_SOURCE = "/CRMSystem;component/Images/DefaultAccountFemaleImg.png";

        private User _currentUser;
        private Address _currentUserAddress;
        private bool _maleMode;

        public AccountViewModel(BaseViewModel view) : base(view)
        {
            ChangeUserImage = new RelayCommand(ChangeDefaultAccountImage);
            EditPersonalData = new RelayCommand(EditUserData);
            _currentUser = CurrentUser ?? new User {UserAddress = new Address(), DateOfBirth = DateTime.Now};
            _currentUserAddress = _currentUser.UserAddress;
            AddNewUserCommand = new RelayCommand(AddNewUser);
            _maleMode = true;
            //AdminModeEnabled = _currentUser.Role == UserRoles.Administrator;
        }

        #region Properties

        private string _accountImageSource;
        public string AccountImageSource
        {
            get
            {
                if (!string.IsNullOrEmpty(_accountImageSource))
                {
                    return _accountImageSource;
                }
                return DEFAULT_MALE_ACCOUNT_IMAGE_SOURCE;

            }
            set { _accountImageSource = value; }
        }

        public string FullName
        {
            get
            {
                return $"{_currentUser.FirstName}  {_currentUser.SecondName}";

            }
        }

        public string Profession
        {
            get
            {
                return _currentUser.Profession.ToString();

            }
        }

        public string PhoneNo
        {
            get
            {
                return _currentUser.PhoneNo;

            }
        }
        public string Email
        {
            get
            {
                return _currentUser.Email;

            }
        }
        public string Age
        {
            get
            {
                var dateOfBirth = _currentUser.DateOfBirth;
                string years;
                try
                {
                    years = DateTime.Now.AddYears(-dateOfBirth.Year).Year.ToString();
                }
                catch (Exception)
                {
                    years = "0";
                }
              
                return $"{years} years";
            }
        }

        public string CreationDate
        {
            get
            {
                return _currentUser.CreationDate.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

            }
        }
        public string Role
        {
            get
            {
                return _currentUser.Role.ToString();

            }
        }

        public string HomeNoExtStreet
        {
            get
            {
                return _currentUserAddress.GetStreetHomeNoExt();

            }
        }
        public string ZipCodeCity
        {
            get
            {
                if (_currentUserAddress != null)
                {
                    return $"{_currentUserAddress.ZipCode} {_currentUserAddress.City}";
                }

                return string.Empty;
            }
        }
        public string Country
        {
            get
            {
                return _currentUserAddress?.Country;

            }
        }

        public string Description
        {
            get
            {
                return _currentUser.Description;

            }
        }
        #endregion

        #region Private methods


        #endregion

        #region ICommand Members and related methods

        public ICommand AddUserCommand { get; set; }
        public ICommand ChangeUserImage { get; set; }
        public ICommand EditPersonalData { get; set; }
        public ICommand AddNewUserCommand { get; set; }

        private void AddNewUser()
        {
            BaseView.SelectedViewModel = new EditAccountViewModel(BaseView, null);
        }

        private void ChangeDefaultAccountImage()
        {
            _maleMode = !_maleMode;
            AccountImageSource = _maleMode ? DEFAULT_MALE_ACCOUNT_IMAGE_SOURCE : DEFAULT_FEMALE_ACCOUNT_IMAGE_SOURCE;
        }

        private void EditUserData()
        {
            BaseView.SelectedViewModel = new EditAccountViewModel(BaseView, CurrentUser);
        }

        #endregion
    }
}
