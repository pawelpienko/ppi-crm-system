﻿using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;

namespace CRMSystem.ViewModels.CustomerViewModels
{
    public class CustomerInfoViewModel : AllCustomersViewModel
    {
        public bool EditEnabled { get; set; }
        private Customer _customerModel;
        private BaseViewModel _viewModel;

        public Customer CurrentCustomer { get; set; }
        public CustomerInfoViewModel(BaseViewModel view, Customer selectedCustomer) : base(view)
        {
            _viewModel = view;
            var customer = selectedCustomer ?? new Customer();
            _customerModel = InstancesHelper.Clone(customer);
            CurrentCustomer = customer;
            AddEventCommand = new RelayCommand(GoToEventView);
            EnableEditingCommand = new RelayCommand(EnableEditing);
            SaveChangesCommand = new RelayCommand(SaveChanges);
            DeleteCustomerCommand = new RelayCommand(DeleteCustomer);
            DiscardChangesCommand = new RelayCommand(DiscardChanges);
            EditEnabled = false;
        }

        public ICommand AddEventCommand { get; set; }
        public ICommand EnableEditingCommand { get; set; }
        public ICommand SaveChangesCommand { get; set; }
        public ICommand DeleteCustomerCommand { get; set; }
        public ICommand DiscardChangesCommand { get; set; }

        private void GoToEventView()
        {
            BaseView.SelectedViewModel = new AddEventViewModel(_viewModel, Cubes.Customers);
        }

        private void EnableEditing()
        {
            EditEnabled = true;
        }

        private void SaveChanges()
        {
            if (_customerModel != null)
            {
                DBHelper.UpdateListOfCustomers(_customerModel);
            }

            BaseView.SelectedViewModel = new AllCustomersViewModel(_viewModel, _customerModel);
            EditEnabled = false;
        }

        private void DiscardChanges()
        {
            BaseView.SelectedViewModel = new AllCustomersViewModel(_viewModel, CurrentCustomer);
            EditEnabled = false;
        }

        private void DeleteCustomer()
        {
            if (_customerModel != null)
            {
                DBHelper.DeleteCustomer(_customerModel.CustomerId);
            }
            BaseView.SelectedViewModel = new AllCustomersViewModel(_viewModel);
        }

        public string CompanyName
        {
            get
            {
                return _customerModel.CompanyName;
            }
            set
            {
                _customerModel.CompanyName = value; 

            }
        }

        public string Description
        {
            get
            {
                return _customerModel.Details;
            }
            set
            {
                _customerModel.Details = value;

            }
        }

        public string StreetHouseNo
        {
            get
            {
                var address = _customerModel.Address;
                var houseNoExt = string.IsNullOrEmpty(address.HomeExt) ? address.HomeNo : $"{address.HomeNo}/{address.HomeExt}";
                return $"{address.Street} {houseNoExt}".Trim();
            }
            set
            {
                _customerModel.Address.Street = value;
            }
        }

        public string ZipCode
        {
            get
            {
                return _customerModel.Address.ZipCode;
            }
            set
            {
                _customerModel.Address.ZipCode = value;

            }
        }
        public string City
        {
            get
            {
                return _customerModel.Address.City;
            }
            set
            {
                _customerModel.Address.City = value;

            }
        }
        public string Country
        {
            get
            {
                return _customerModel.Address.Country;
            }
            set
            {
                _customerModel.Address.Country = value;

            }
        }

        public string ContactPersonName
        {
            get
            {
                var contactPerson = _customerModel.ContactPerson;
                return $"{contactPerson.Name} {contactPerson.SecondName}";
            }
            set
            {
                _customerModel.ContactPerson.Name = value;

            }
        }

        public string Phone
        {
            get
            {
                return _customerModel.ContactPerson.Phone;
            }
            set
            {
                _customerModel.ContactPerson.Phone = value;

            }
        }
        public string Profession
        {
            get
            {
                return _customerModel.ContactPerson.Profession.ToString();
            }
            set
            {
              //  CurrentCustomer.ContactPerson.Profession = value;
            }
        }
    }
}
