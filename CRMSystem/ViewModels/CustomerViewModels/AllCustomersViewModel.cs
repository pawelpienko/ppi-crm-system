﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;
using CRMSystem.ViewModels.CustomerViewModels;
using CRMSystem.ViewModels.SupplierViewModels;

namespace CRMSystem.ViewModels
{
    public class AllCustomersViewModel : BaseViewModel
    {
        public BaseViewModel SelectedCustomerViewModel { get; set; }
        private BaseViewModel _viewModel;
        public ICommand EnterSearchInputCommand { get; set; }
        public ICommand AddCustomerCommand { get; set; }

        public AllCustomersViewModel(BaseViewModel view) : base(view)
        {
            //AddEventCommand = new RelayCommand(ShowAddEventView);
            _viewModel = view;
            EnterSearchInputCommand = new RelayCommand(EnterSearchInput);
            AddCustomerCommand = new RelayCommand(AddCustomer);
        }

        public AllCustomersViewModel(BaseViewModel view, Customer selectedCustomer) : base(view)
        {
            _viewModel = view;
            AddCustomerCommand = new RelayCommand(AddCustomer);
            SelectedCustomerViewModel = new CustomerInfoViewModel(view, selectedCustomer);
        }

        private Customer _selectedItem;

        public Customer SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                
                SelectedCustomerViewModel = new CustomerInfoViewModel(_viewModel, value);
            }
        }

        private ObservableCollection<Customer> _customers;
        private string _searchInput;
        public string SearchInput
        {
            get
            {
                return _searchInput;

            }
            set
            {
                _searchInput = value;
                if (string.IsNullOrEmpty(value))
                {
                    EnterSearchInput();
                }
            }
        }

        public ObservableCollection<Customer> Customers
        {
            get
            {
                if (_customers == null)
                {
                    _customers = new ObservableCollection<Customer>();
                    var allCustomers = DBHelper.ListOfCustomers;
                    foreach (var customer in allCustomers)
                    {
                        _customers.Add(customer);
                    }
                }

                return _customers;
            }
            set
            {
                _customers = value;
            }
        }

        private void EnterSearchInput()
        {
            var selected = DBHelper.ListOfCustomers.Where(n => n.CompanyName.ToLower().Contains(SearchInput.ToLower())).ToList();
            Customers = new ObservableCollection<Customer>(selected);
        }

        private void AddCustomer()
        {
            DBHelper.AddCustomer("New customer");
            BaseView.SelectedViewModel = new AllCustomersViewModel(_viewModel);
        }
    }
}
