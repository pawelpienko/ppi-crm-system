﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;

namespace CRMSystem.ViewModels.SupplierViewModels
{
    public class AllSuppliersViewModel : BaseViewModel
    {
        public BaseViewModel SelectedSupplierViewModel { get; set; }
        private BaseViewModel _viewModel;
        public ICommand EnterSearchSupplierInputCommand { get; set; }
        public ICommand AddSupplierCommand { get; set; }

        public AllSuppliersViewModel(BaseViewModel view) : base(view)
        {
            _viewModel = view;
            EnterSearchSupplierInputCommand = new RelayCommand(EnterSearchInput);
            AddSupplierCommand = new RelayCommand(AddSupplier);
        }

        public AllSuppliersViewModel(BaseViewModel view, Supplier selectedSupplier) : base(view)
        {
            _viewModel = view;
            SelectedSupplierViewModel = new SupplierInfoViewModel(view, selectedSupplier);
            EnterSearchSupplierInputCommand = new RelayCommand(EnterSearchInput);
            AddSupplierCommand = new RelayCommand(AddSupplier);
        }

        private Supplier _selectedItem;

        public Supplier SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;

                SelectedSupplierViewModel = new SupplierInfoViewModel(_viewModel, value);
            }
        }

        private ObservableCollection<Supplier> _suppliers;
        private string _searchInput;
        public string SearchInput
        {
            get
            {
                return _searchInput;

            }
            set
            {
                _searchInput = value;
                if (string.IsNullOrEmpty(value))
                {
                    EnterSearchInput();
                }
            }
        }

        public ObservableCollection<Supplier> Suppliers
        {
            get
            {
                if (_suppliers == null)
                {
                    _suppliers = new ObservableCollection<Supplier>();
                    var allSuppliers = DBHelper.ListOfSuppliers;
                    foreach (var supplier in allSuppliers)
                    {
                        _suppliers.Add(supplier);
                    }
                }

                return _suppliers;
            }
            set
            {
                _suppliers = value;
            }
        }

        private void EnterSearchInput()
        {
            var selected = DBHelper.ListOfSuppliers.Where(n => n.SupplierCompanyName.ToLower().Contains(SearchInput.ToLower())).ToList();
            Suppliers = new ObservableCollection<Supplier>(selected);
        }

        private void AddSupplier()
        {
            DBHelper.AddSupplier("New supplier");
            BaseView.SelectedViewModel = new AllSuppliersViewModel(_viewModel);
        }
    }
}
