﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;

namespace CRMSystem.ViewModels.SupplierViewModels
{
    public class SupplierInfoViewModel : AllSuppliersViewModel
    {
        public bool EditEnabled { get; set; }
        private Supplier _supplierModel;
        private BaseViewModel _viewModel;

        private Product _selectedProduct;

        public Product SelectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                _selectedProduct = value;
            }
        }


        public Supplier CurrentSupplier { get; set; }

        public SupplierInfoViewModel(BaseViewModel view, Supplier selectedSupplier) : base(view)
        {
            _viewModel = view;
            var supplier = selectedSupplier ?? new Supplier();
            _supplierModel = InstancesHelper.Clone(supplier);
            CurrentSupplier = supplier;
            
            EnableEditingCommand = new RelayCommand(EnableEditing);
            SaveChangesCommand = new RelayCommand(SaveChanges);
            DeleteSupplierCommand = new RelayCommand(DeleteSupplier);
            DiscardChangesCommand = new RelayCommand(DiscardChanges);
            AddNewProductCommand = new RelayCommand(AddNewProduct);
            RemoveProductCommand = new RelayCommand(DeleteProduct);
            //RefreshPriceCommand = new RelayCommand(RefreshPrice);
            EditEnabled = false;
        }
        public ICommand AddNewProductCommand { get; set; }
        public ICommand RemoveProductCommand { get; set; }
        public ICommand EnableEditingCommand { get; set; }
        public ICommand SaveChangesCommand { get; set; }
        public ICommand DeleteSupplierCommand { get; set; }
        public ICommand DiscardChangesCommand { get; set; }
        public ICommand RefreshPriceCommand { get; set; }
        private void EnableEditing()
        {
            EditEnabled = true;
        }

        private void SaveChanges()
        {
            if (_supplierModel != null)
            {
                DBHelper.UpdateListOfSuppliers(_supplierModel);
            }

            BaseView.SelectedViewModel = new AllSuppliersViewModel(_viewModel, _supplierModel);
            EditEnabled = false;
        }

        private void DiscardChanges()
        {
            BaseView.SelectedViewModel = new AllSuppliersViewModel(_viewModel, CurrentSupplier);
            EditEnabled = false;
        }

        private void DeleteSupplier()
        {
            if (_supplierModel != null)
            {
                DBHelper.DeleteSupplier(_supplierModel.SupplierId);
            }
            BaseView.SelectedViewModel = new AllSuppliersViewModel(_viewModel);
        }

        private void AddNewProduct()
        {
            Products.Add(new Product());
        }

        private void DeleteProduct()
        {
            if (SelectedProduct == null)
            {
                return;
            }
            var product = _supplierModel.SupplierProducts.FirstOrDefault(n => n.ProductId == SelectedProduct.ProductId);
            _supplierModel.SupplierProducts.Remove(product);
            Products = _supplierModel.SupplierProducts;
        }

        private ObservableCollection<Product> _products;

        public ObservableCollection<Product> Products
        {
            get
            {
                if (_supplierModel == null)
                {
                    return null;
                }

                return _supplierModel.SupplierProducts;
            }
            set
            {
                if (_supplierModel == null)
                {
                    return;
                }

                _supplierModel.SupplierProducts = value;
            }
        }

        public string CompanyName
        {
            get
            {
                return _supplierModel.SupplierCompanyName;
            }
            set
            {
                _supplierModel.SupplierCompanyName = value;

            }
        }

        public string Description
        {
            get
            {
                return _supplierModel.Details;
            }
            set
            {
                _supplierModel.Details = value;

            }
        }

        public string StreetHouseNo
        {
            get
            {
                var address = _supplierModel.Address;
                var houseNoExt = string.IsNullOrEmpty(address.HomeExt) ? address.HomeNo : $"{address.HomeNo}/{address.HomeExt}";
                return $"{address.Street} {houseNoExt}".Trim();
            }
            set
            {
                _supplierModel.Address.Street = value;
            }
        }

        public string ZipCode
        {
            get
            {
                return _supplierModel.Address.ZipCode;
            }
            set
            {
                _supplierModel.Address.ZipCode = value;

            }
        }
        public string City
        {
            get
            {
                return _supplierModel.Address.City;
            }
            set
            {
                _supplierModel.Address.City = value;

            }
        }
        public string Country
        {
            get
            {
                return _supplierModel.Address.Country;
            }
            set
            {
                _supplierModel.Address.Country = value;

            }
        }

        public string ContactPersonName
        {
            get
            {
                var contactPerson = _supplierModel.ContactPerson;
                return $"{contactPerson.Name} {contactPerson.SecondName}";
            }
            set
            {
                _supplierModel.ContactPerson.Name = value;
            }
        }

        public string Phone
        {
            get
            {
                return _supplierModel.ContactPerson.Phone;
            }
            set
            {
                _supplierModel.ContactPerson.Phone = value;

            }
        }
        public string Profession
        {
            get
            {
                return _supplierModel.ContactPerson.Profession.ToString();
            }
            set
            {
                //  CurrentSupplier.ContactPerson.Profession = value;
            }
        }
    }
}
