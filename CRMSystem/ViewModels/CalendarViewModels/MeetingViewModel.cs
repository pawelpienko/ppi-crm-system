﻿using System;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;

namespace CRMSystem.ViewModels
{
    public class MeetingViewModel : BaseViewModel
    {
        public ICommand AddNewMeetingCommand { get; set; }
        public ICommand DiscardChangesCommand { get; set; }
        protected Meeting MeetingModel;

        public MeetingViewModel(BaseViewModel view) : base(view)
        {
            AddNewMeetingCommand = new RelayCommand(SaveChanges);
            DiscardChangesCommand = new RelayCommand(DiscardChanges);
            MeetingModel = new Meeting();
        }

        protected void GoToEventCalendarView()
        {
            BaseView.SelectedViewModel = new CalendarViewModel(BaseView);
        }

        internal virtual void SaveChanges()
        {
            if (!string.IsNullOrEmpty(MeetingTitle))
            {
                DBHelper.AddMeeting(MeetingModel);
                GoToEventCalendarView();
            }
        }

        private void DiscardChanges()
        {
            GoToEventCalendarView();
        }

        public string MeetingDescription
        {
            get { return MeetingModel.Description; }
            set { MeetingModel.Description = value; }
        }

        public DateTime MeetingExecutionDate
        {
            get
            {
                return MeetingModel.Date;
            }
            set
            {
                MeetingModel.Date = value;
                MeetingModel.TimeString = value.ToString("HH:mm");
            }
        }

        private DateTime _meetingTime;

        public DateTime MeetingTime
        {
            get
            {
                _meetingTime = MeetingModel.Date;
                return _meetingTime;

            }
            set
            {
                var date = MeetingModel.Date;
                _meetingTime = new DateTime(date.Year, date.Month, date.Day, value.Hour, value.Minute, value.Second);
                MeetingModel.Date = _meetingTime;
            }
        }

        public string MeetingTitle
        {
            get { return MeetingModel.Title; }
            set { MeetingModel.Title = value; }
        }

        
    }
}
