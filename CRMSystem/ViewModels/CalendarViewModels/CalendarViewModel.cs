﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;
using CRMSystem.ViewModels.CalendarViewModels;

namespace CRMSystem.ViewModels
{
    public class CalendarViewModel : BaseViewModel
    {
        public ICommand PreviousWeekCommand { get; set; }
        public ICommand NextWeekCommand { get; set; }
        public ICommand AddNewMeetingCommand { get; set; }
        public ICommand EditMeetingCommand { get; set; }

        private Meeting _selectedMeeting;

        public Meeting SelectedItem
        {
            get
            {
                if (_selectedMeeting != null)
                {
                    BaseView.SelectedViewModel = new EditMeetingViewModel(BaseView, _selectedMeeting);
                }
                return _selectedMeeting;
            }
            set { _selectedMeeting = value; }
        }

        private List<Meeting> _meetings;
        public CalendarViewModel(BaseViewModel view) : base(view)
        {
            EditMeetingCommand = new RelayCommand(ShowEditMeetingView);
            PreviousWeekCommand = new RelayCommand(GetPreviousWeekDates);
            NextWeekCommand = new RelayCommand(GetNextsWeekDates);
            AddNewMeetingCommand = new RelayCommand(ShowAddMeetingView);
            CurrentDateToDisplay = DateTime.Today;
            _meetings = DBHelper.GetAllMeetings();
        }

        public DateTime CurrentDateToDisplay { get; set; }

        private void ShowEditMeetingView()
        {
            BaseView.SelectedViewModel = new EditMeetingViewModel(BaseView, SelectedItem);
        }

        private void ShowAddMeetingView()
        {
            BaseView.SelectedViewModel = new MeetingViewModel(BaseView);
        }

        private void GetPreviousWeekDates()
        {
            CurrentDateToDisplay = CurrentDateToDisplay.AddDays(-7);
            
        }

        private void GetNextsWeekDates()
        {
            CurrentDateToDisplay = CurrentDateToDisplay.AddDays(7);
        }



        private ObservableCollection<Meeting> _mondayMeetings;
        public ObservableCollection<Meeting> MondayMeetings
        {
            get
            {
                _mondayMeetings = new ObservableCollection<Meeting>();
                AssignMeetingsByDay(DayOfWeek.Monday, _mondayDate, _mondayMeetings);
                return _mondayMeetings;
            }
        }

        private ObservableCollection<Meeting> _tuesdayMeetings;

        public ObservableCollection<Meeting> TuesdayMeetings
        {
            get
            {
                _tuesdayMeetings = new ObservableCollection<Meeting>();
                AssignMeetingsByDay(DayOfWeek.Tuesday, _tuesdayDate, _tuesdayMeetings);

                return _tuesdayMeetings;
            }
        }

        private ObservableCollection<Meeting> _wednesdayMeetings;

        public ObservableCollection<Meeting> WednesdayMeetings
        {
            get
            {
                _wednesdayMeetings = new ObservableCollection<Meeting>();
                AssignMeetingsByDay(DayOfWeek.Wednesday, _wednesdayDate, _wednesdayMeetings);

                return _wednesdayMeetings;
            }
        }

        private ObservableCollection<Meeting> _thursdayMeetings;

        public ObservableCollection<Meeting> ThursdayMeetings
        {
            get
            {
                _thursdayMeetings = new ObservableCollection<Meeting>();
                AssignMeetingsByDay(DayOfWeek.Thursday, _thursdayDate, _thursdayMeetings);

                return _thursdayMeetings;
            }
        }

        private ObservableCollection<Meeting> _fridayMeetings;

        public ObservableCollection<Meeting> FridayMeetings
        {
            get
            {
                _fridayMeetings = new ObservableCollection<Meeting>();
                AssignMeetingsByDay(DayOfWeek.Friday, _fridayDate, _fridayMeetings);

                return _fridayMeetings;
            }
        }

        private ObservableCollection<Meeting> _sundayMeetings;

        public ObservableCollection<Meeting> SundayMeetings
        {
            get
            {
                _sundayMeetings = new ObservableCollection<Meeting>();
                AssignMeetingsByDay(DayOfWeek.Sunday, _sundayDate, _sundayMeetings);

                return _sundayMeetings;
            }
        }

        private ObservableCollection<Meeting> _saturdayMeetings;

        public ObservableCollection<Meeting> SaturdayMeetings
        {
            get
            {
                _saturdayMeetings = new ObservableCollection<Meeting>();
                AssignMeetingsByDay(DayOfWeek.Saturday, _saturdayDate, _saturdayMeetings);

                return _saturdayMeetings;
            }
        }

        private DateTime _mondayDate;
        public string MondayDate {
            get
            {
                _mondayDate = GetFormattedDateByDay(DayOfWeek.Monday);
                return _mondayDate.ToString("yyyy/MM/dd");

            }
            set { }
        }

        private DateTime _tuesdayDate;

        public string TuesdayDate
        {
            get
            {
                _tuesdayDate = GetFormattedDateByDay(DayOfWeek.Tuesday);
                return _tuesdayDate.ToString("yyyy/MM/dd");

            }
            set { }
        }

        private DateTime _wednesdayDate;

        public string WednesdayDate
        {
            get
            {
                _wednesdayDate = GetFormattedDateByDay(DayOfWeek.Wednesday);
                return _wednesdayDate.ToString("yyyy/MM/dd");

            }
            set { }
        }

        private DateTime _thursdayDate;
        public string ThursdayDate
        {
            get
            {
                _thursdayDate = GetFormattedDateByDay(DayOfWeek.Thursday);
                return _thursdayDate.ToString("yyyy/MM/dd");

            }
            set { }
        }

        private DateTime _fridayDate;
        public string FridayDate
        {
            get
            {
                _fridayDate = GetFormattedDateByDay(DayOfWeek.Friday);
                return _fridayDate.ToString("yyyy/MM/dd");

            }
            set { }
        }

        private DateTime _sundayDate;
        public string SundayDate
        {
            get
            {
                _sundayDate = GetFormattedDateByDay(DayOfWeek.Sunday);
                return _sundayDate.ToString("yyyy/MM/dd");

            }
            set { }
        }


        private DateTime _saturdayDate;
        public string SaturdayDate
        {
            get
            {
                _saturdayDate = GetFormattedDateByDay(DayOfWeek.Saturday);
                return _saturdayDate.ToString("yyyy/MM/dd");

            }
            set { }
        }


        private DateTime GetFormattedDateByDay(DayOfWeek dayOfWeek)
        {
            if (dayOfWeek == DayOfWeek.Sunday)
            {
                var sunday = CurrentDateToDisplay.AddDays(-(int)DateTime.Today.DayOfWeek + (int)dayOfWeek).AddDays(7);
                return sunday;
            }
             
            var day = CurrentDateToDisplay.AddDays(-(int)DateTime.Today.DayOfWeek + (int)dayOfWeek);
            return day;
        }

        private void AssignMeetingsByDay(DayOfWeek day, DateTime date, ObservableCollection<Meeting> listToFill)
        {
            var meetings = _meetings.FindAll(n => n.Date.Date == date.Date.Date && n.Date.DayOfWeek == day);
            var sortedMeetings = meetings.OrderBy(n => n.Date);

            foreach (var meeting in sortedMeetings)
            {
                listToFill.Add(meeting);
            }
        }
    }
}
