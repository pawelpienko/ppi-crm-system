﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;
using CRMSystem.PdfDocumentManager;

namespace CRMSystem.ViewModels.CalendarViewModels
{
    public class EditMeetingViewModel : MeetingViewModel
    {
        public ICommand DeleteMeetingCommand { get; set; }
        public EditMeetingViewModel(BaseViewModel view, Meeting currentMeeting) : base(view)
        {
            MeetingModel = currentMeeting;
            DeleteMeetingCommand = new RelayCommand(DeleteMeeting);
        }

        private void DeleteMeeting()
        {
            if (MeetingModel != null)
            {
                DBHelper.DeleteMeeting(MeetingModel.MeetingId);
                GoToEventCalendarView();
            }
        }

        internal override void SaveChanges()
        {
            if (!string.IsNullOrEmpty(MeetingTitle))
            {
                DBHelper.AddMeeting(MeetingModel);
                GoToEventCalendarView();
            }
        }
    }
}
