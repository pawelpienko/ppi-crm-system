﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMSystem.Helpers;
using CRMSystem.Models;
using LiveCharts;
using LiveCharts.Definitions.Series;
using LiveCharts.Wpf;
using LiveCharts.Wpf.Charts.Base;
using MindFusion.Scheduling.Wpf;

namespace CRMSystem.ViewModels.ChartsViewModels
{
    public class ChartsViewModel : BaseViewModel
    {
        private const string CountPerEventTypeOptionPie = "Events count / types";
        //private const string EventsCountPerCityOptionPie = "Events count / city";
        private const string ProductsCountPerSupplierOptionPie = "Products count / supplier";
        private const string EventsTotalPriceOptionPie = "Total price / event";

        private const string EventsCountPerMonthCartesian = "Events count / month";
        private const string MeetingsCountPerMonthCartesian = "Meetings count / month";
        private const string NewCustomersCountPerMonthCartesian = "New customers count / month";
        private const string NewSuppliersCountPerMonthCartesian = "New suppliers count / month";


        private BaseViewModel BaseViewModel { get; set; }

        private List<Tuple<Months, List<double>>> GetDataForCartesianChart(IEnumerable<object> collection)
        {
            var data = new List<Tuple<Months, List<double>>>();
            var months = (Months[]) Enum.GetValues(typeof(Months));

            if (collection is List<Event> events && events.Count > 0)
            {
                foreach (var month in months)
                {
                    var count = events.Count(n => n.EventExecutionDate.ToString("MMM").Equals(month.ToString()));
                    data.Add(new Tuple<Months, List<double>>(month, new List<double> { count }));
                }
                return data;
            }
            if (collection is List<Meeting> meetings && meetings.Count > 0)
            {
                foreach (var month in months)
                {
                    var count = meetings.Count(n => n.Date.ToString("MMM").Equals(month.ToString()));
                    data.Add(new Tuple<Months, List<double>>(month, new List<double> { count }));
                }
                return data;
            }

            if (collection is List<Customer> customers && customers.Count > 0)
            {
                int summary = 0;
                foreach (var month in months)
                {
                    summary += customers.Count(n => n.CreationDate.ToString("MMM").Equals(month.ToString()));
                    data.Add(new Tuple<Months, List<double>>(month, new List<double> { summary }));
                }
            }
            if (collection is List<Supplier> suppliers && suppliers.Count > 0)
            {
                int summary = 0;
                foreach (var month in months)
                {
                    summary += suppliers.Count(n => n.CreationDate.ToString("MMM").Equals(month.ToString()));
                    data.Add(new Tuple<Months, List<double>>(month, new List<double> { summary }));
                }
            }

            return data;
        }

        private SeriesCollection GetCartesianChartSeriesCollection(List<Tuple<Months, List<double>>> titleAndValuesTuple)
        {
            var cartesianChartSeriesCollection = new SeriesCollection();
            var values = new ChartValues<double>();
            foreach (var (item1, item2) in titleAndValuesTuple)
            {
                
                values.AddRange(item2);
                
            }
            cartesianChartSeriesCollection.Add(new LineSeries
            {
                Title = "Data",
                Values = values
            });
            //CartesianChartSeriesCollection = new SeriesCollection
            //{
            //    //new LineSeries
            //    //{
            //    //    Title = "Ventas", Values = new ChartValues<double>{ 400, 600, 500, 500, 0, 0, 122, 333, 400, 100, 900, 310 }
            //    //},
            //    //new LineSeries
            //    //{
            //    //    Title = "eloelo", Values = new ChartValues<double>{ 100, 200, 550, 102, 500, 100, 200, 550, 102, 500, 0, 700 }
            //    //}
            //};

            Labels = Enum.GetNames(typeof(Months));
            yFormatter = value => value.ToString("C");

            return cartesianChartSeriesCollection;
        }

        public Func<double, string> yFormatter { get; set; }


        public string[] Labels { get; set; }

        private ObservableCollection<string> _cartesianChartOptions;

        public ObservableCollection<string> CartesianChartOptions
        {
            get
            {
                if (_cartesianChartOptions == null)
                {
                    _cartesianChartOptions = new ObservableCollection<string>
                    {
                        EventsCountPerMonthCartesian,
                        MeetingsCountPerMonthCartesian,
                        NewCustomersCountPerMonthCartesian,
                        NewSuppliersCountPerMonthCartesian
                    };
                }

                return _cartesianChartOptions;
            }
            set
            {
                _cartesianChartOptions = value;

            }
        }


        //PieChart
        private string _selectedPieChartOption;

        public string SelectedPieChartOption
        {
            get
            {
                return string.IsNullOrEmpty(_selectedPieChartOption) ? ProductsCountPerSupplierOptionPie : _selectedPieChartOption;
            }
            set
            {
                _selectedPieChartOption = value;
                MyPieChartModel = null;
            }
        }

        private string _selectedCartesianChartOption;

        public string SelectedCartesianChartOption
        {
            get
            {
                return string.IsNullOrEmpty(_selectedCartesianChartOption) ? MeetingsCountPerMonthCartesian : _selectedCartesianChartOption;
            }
            set
            {
                _selectedCartesianChartOption = value;
                CartesianChartSeriesCollection = null;
            }
        }


        private ObservableCollection<string> _pieChartOptions;

        public ObservableCollection<string> PieChartOptions
        {
            get
            {
                if (_pieChartOptions == null)
                {
                    _pieChartOptions = new ObservableCollection<string>
                    {
                        CountPerEventTypeOptionPie,
                        ProductsCountPerSupplierOptionPie,
                        EventsTotalPriceOptionPie
                    };
                }

                return _pieChartOptions;
            }
            set
            {
                _pieChartOptions = value;

            }
        }

        private PieChart _myPieChart;
        public PieChart MyPieChartModel
        {
            get
            {
                if (_myPieChart != null)
                {
                    return _myPieChart;
                }

                List<Tuple<string, double>> data;
                switch (SelectedPieChartOption)
                {
                    case CountPerEventTypeOptionPie:
                        data = GetDataEventTypeCount();
                        break;
                    case ProductsCountPerSupplierOptionPie:
                        data = GetDataProductsCount();
                        break;
                    default:
                        data = GetDataEventsPrices();
                        break;
                }
                
                return PopulatePieChart(data);

            }
            set { _myPieChart = value; }
        }

        private SeriesCollection _cartesianSeriesCollection;
        public SeriesCollection CartesianChartSeriesCollection
        {
            get
            {
                if (_cartesianSeriesCollection == null)
                {
                    IEnumerable<object> data;
                    switch (SelectedCartesianChartOption)
                    {
                        case MeetingsCountPerMonthCartesian:
                            data = DBHelper.ListOfMeetings;
                            break;
                        case NewCustomersCountPerMonthCartesian:
                            data = DBHelper.ListOfCustomers;
                            break;
                        case NewSuppliersCountPerMonthCartesian:
                            data = DBHelper.ListOfSuppliers;
                            break;
                        default:
                            data = DBHelper.ListOfEvents;
                            break;
                    }
                    _cartesianSeriesCollection = GetCartesianChartSeriesCollection(GetDataForCartesianChart(data));
                }
                
                return _cartesianSeriesCollection;
            }
            set
            {
                _cartesianSeriesCollection = value;

            }
        }

        private static List<Tuple<string, double>> GetDataEventsPrices()
        {
            var events = DBHelper.ListOfEvents;
            var eventsTitles = events.Select(n => n.EventTitle).Distinct();
            var list = new List<Tuple<string, double>>();
            foreach (var title in eventsTitles)
            {
                var products = events.FirstOrDefault(n => n.EventTitle.Equals(title))?.Products;
                if (products == null || products.Count <= 0) 
                {
                    continue;
                }

                var totalPrize = products.Sum(n => n.Price);
                list.Add(new Tuple<string, double>($"{title} zł", decimal.ToDouble(totalPrize)));
            }

            return list;
        }

        private static List<Tuple<string, double>> GetDataProductsCount()
        {
            var allSuppliers = DBHelper.ListOfSuppliers;
            var suppliers = allSuppliers.Select(item => item.SupplierCompanyName).Distinct();
            var list = new List<Tuple<string, double>>();
            foreach (var supplier in suppliers)
            {
                var products = allSuppliers.FirstOrDefault(n => n.SupplierCompanyName == supplier)?.SupplierProducts;
                if (products != null)
                {
                    list.Add(new Tuple<string, double>(supplier, products.Count));
                }
            }

            return list;
        }

        private static List<Tuple<string, double>> GetDataEventTypeCount()
        {
            var list = new List<Tuple<string, double>>();
            foreach (var type in (EventType[])Enum.GetValues(typeof(EventType)))
            {
                var typeCount = DBHelper.ListOfEvents.Count(n => n.EventType == type);
                if (typeCount > 0)
                {
                    list.Add(new Tuple<string, double>(type.ToString(), typeCount));
                }
            }

            return list;
        }

        private static List<Tuple<string, double>> GetDataEventCityCount()
        {
            var allEvents = DBHelper.ListOfEvents;
            var cities = allEvents.Select(item => item.EventAddress.City).Distinct();
            var list = cities.Select(city => new Tuple<string, double>(city, allEvents.Count(n => n.EventAddress.City == city))).ToList();

            return list;
        }

        private PieChart PopulatePieChart(IReadOnlyCollection<Tuple<string, double>> titleValueTupleList)
        {
            if (titleValueTupleList == null)
            {
                return new PieChart();
            }

            var seriesCollection = new SeriesCollection();

            foreach (var item in titleValueTupleList)
            {
                seriesCollection.Add(new PieSeries
                {
                    Title = item.Item1,
                    Values = new ChartValues<double> { item.Item2 },
                    DataLabels = true,
                    LabelPoint = PointLabel
                });
            }
            _myPieChart = new PieChart();
            _myPieChart.Series = seriesCollection;
            _myPieChart.LegendLocation = LegendLocation.Bottom;

            return _myPieChart;
        }

        public ChartsViewModel(BaseViewModel view) : base(view)
        {
            BaseViewModel = view;
            DataContext();
            //PopulateCartesianChart(GetDataForCartesianChart(DBHelper.ListOfMeetings));
        }

        public Func<ChartPoint, string> PointLabel { get; set; }

        public void DataContext()
        {
            PointLabel = chartPoint => $"{chartPoint.Y} ({chartPoint.Participation:P})";
        }
    }

    public enum Months
    {
        Jan,
        Feb,
        Mar,
        Apr,
        May,
        Jun,
        Jul,
        Aug,
        Sep,
        Oct,
        Nov,
        Dec
    }
}