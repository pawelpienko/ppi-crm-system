﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;
using CRMSystem.PdfDocumentManager;
using CRMSystem.ViewModels.AllProductsViewModels;
using Task = CRMSystem.Models.Task;

namespace CRMSystem.ViewModels
{
    public class EditEventViewModel : BaseViewModel
    {
        private Event _selectedEvent;
        private Task _selectedTask;
        private Product _selectedProduct;

        public EditEventViewModel(BaseViewModel view, Event selectedEvent) : base(view)
        {
            _selectedEvent = InstancesHelper.Clone(selectedEvent);
            DiscardChangesCommand = new RelayCommand(DiscardChanges);
            SaveChangesCommand = new RelayCommand(SaveChanges);
            AddNewTaskCommand = new RelayCommand(AddTask);
            RemoveTaskCommand = new RelayCommand(RemoveTask);
            RemoveEventCommand = new RelayCommand(RemoveEvent);
            RemoveProductCommand = new RelayCommand(RemoveProduct);
            AddNewProductCommand = new RelayCommand(ShowProductsView);
            GenerateProductsPdf = new RelayCommand(GeneratePdf);
            SetSummaryPrice(_selectedEvent);
        }


        private Customer _selectedCustomer;
        public Customer SelectedCustomer
        {
            get
            {
                if (_selectedCustomer == null)
                {
                    _selectedCustomer = _selectedEvent.EventCustomer;
                }
                return _selectedCustomer;

            }
            set
            {
                _selectedEvent.EventCustomer = value;
            }
        }

        private ObservableCollection<Customer> _customers;
        public ObservableCollection<Customer> Customers
        {
            get
            {
                if (_customers == null)
                {
                    _customers = new ObservableCollection<Customer>();
                    var allCustomers = DBHelper.ListOfCustomers;
                    foreach (var customer in allCustomers)
                    {
                        _customers.Add(customer);
                    }
                }

                return _customers;
            }
            set
            {
                _customers = value;
            }
        }

        #region Properties

        public string SummaryPrice { get; set; }

        private void ShowProductsView()
        {
            BaseView.SelectedViewModel = new AllProductsViewModel(BaseView, _selectedEvent, Cubes.EventEdit);
        }

        private void SetSummaryPrice(Event currentEvent)
        {
            if (currentEvent == null)
            {
                SummaryPrice = string.Empty;
                return;
            }

            var summary = currentEvent.Products.Sum(n => n.Price);
            SummaryPrice = $"Summary: {summary.ToString("0.00", CultureInfo.CurrentCulture)} zł";
        }

        private void RemoveProduct()
        {
            if (_selectedProduct != null)
            {
                var products = _selectedEvent.Products;
                products.Remove(_selectedProduct);
                SetSummaryPrice(_selectedEvent);
            }
        }

        public Event SelectedEvent
        {
            get
            {
                return _selectedEvent;
            }
            set
            {
                _selectedEvent = value;
                
            }
        }
        public ObservableCollection<Product> SelectedProducts
        {
            get
            {
                return _selectedEvent.Products;

            }
            set
            {
                _selectedEvent.Products = value;
            }
        }


        public int ProgressBarValue { get; set; }

        public ObservableCollection<Task> Tasks
        {
            get
            {
                return _selectedEvent.Tasks;
            }
            set
            {
                
            }
        }

        public Task SelectedTask
        {
            get { return null;}
            set
            {
                _selectedTask = value;
            }
        }
        public Product SelectedProduct
        {
            get { return null; }
            set { _selectedProduct = value; }
        }

        public string EventTitle
        {
            get { return _selectedEvent.EventTitle; }
            set { _selectedEvent.EventTitle = value; }

        }

        public string Description
        {
            get { return _selectedEvent.Description; }
            set { _selectedEvent.Description = value; }

        }

        public DateTime EventExecutionDate
        {
            get { return _selectedEvent.EventExecutionDate; }
            set { _selectedEvent.EventExecutionDate = value; }
        }

        public DateTime EventCreationDate
        {
            get { return _selectedEvent.EventCreationDate;}
            set { _selectedEvent.EventCreationDate = value; }
        }

        public EventStatus Status
        {
            get { return _selectedEvent.Status; }
            set { _selectedEvent.Status = value; }
            
        }

        public IEnumerable<EventStatus> EventStatuses
        {
            get
            {
                return Enum.GetValues(typeof(EventStatus)).Cast<EventStatus>();
            }
            set { }
        }

        #endregion

        #region ICommand Members and methods

        public ICommand DiscardChangesCommand { get; set; }
        public ICommand SaveChangesCommand { get; set; }
        public ICommand RemoveTaskCommand { get; set; }
        public ICommand AddNewTaskCommand { get; set; }
        public ICommand RemoveEventCommand { get; set; }
        public ICommand AddNewProductCommand { get; set; }
        public ICommand RemoveProductCommand { get; set; }
        public ICommand GenerateProductsPdf { get; set; }

        private void GeneratePdf()
        {
            var filePath = GetFilePath();
            if (string.IsNullOrEmpty(filePath) || _selectedEvent == null)
            {
                return;
            }

            new PdfGenerator().CreateProductsPdf(_selectedEvent, filePath);
        }

        private string GetFilePath()
        {
            using (var dialog = new FolderBrowserDialog())
            { 
                dialog.ShowDialog();
                return dialog.SelectedPath;
            }
        }

        private void RemoveTask()
        {
            if (_selectedTask != null)
            {
                _selectedEvent.Tasks.Remove(_selectedTask);
            }
        }

        private void AddTask()
        {
            _selectedEvent.Tasks.Add(new Task());
        }


        private void SaveChanges()
        {
            DBHelper.UpdateListOfEvents(_selectedEvent);
            BaseView.SelectedViewModel = new EventPanelViewModel(BaseView);
        }

        private void DiscardChanges()
        {
            BaseView.SelectedViewModel = new EventPanelViewModel(BaseView);
        }

        private void RemoveEvent()
        {
            DBHelper.DeleteEvent(_selectedEvent.EventId);
            BaseView.SelectedViewModel = new EventPanelViewModel(BaseView);
        }

        #endregion
    }
}
