﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;
using CRMSystem.Views;

namespace CRMSystem.ViewModels
{
    public class EventPanelViewModel : BaseViewModel
    {
        public ICommand AddEventCommand { get; set; }

        public EventPanelViewModel(BaseViewModel view) : base(view)
        {
             AddEventCommand = new RelayCommand(ShowAddEventView);           
        }

        private void ShowEventWindow(Event selectedEvent)
        {
           BaseView.SelectedViewModel = new EditEventViewModel(BaseView, selectedEvent);
        }

        private Event _selectedItem;

        public Event SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                ShowEventWindow(value);
                _selectedItem = null;
            }
        }


        private ObservableCollection<Event> _reportedEvents;

        public ObservableCollection<Event> ReportedEvents
        {
            get
            {
                if (_reportedEvents == null)
                {
                    _reportedEvents = new ObservableCollection<Event>();

                    AssignEventsByStatus(EventStatus.Reported, _reportedEvents);
                }

                return _reportedEvents;
            }
        }

        private ObservableCollection<Event> _underAnalysisEvents;

        public ObservableCollection<Event> UnderAnalysisEvents
        {
            get
            {
                if (_underAnalysisEvents == null)
                {
                    _underAnalysisEvents = new ObservableCollection<Event>();

                    AssignEventsByStatus(EventStatus.UnderAnalysis, _underAnalysisEvents);
                }

                return _underAnalysisEvents;
            }
        }

        private ObservableCollection<Event> _inProgressEvents;

        public ObservableCollection<Event> InProgressEvents
        {
            get
            {
                if (_inProgressEvents == null)
                {
                    _inProgressEvents = new ObservableCollection<Event>();

                    AssignEventsByStatus(EventStatus.InProgress, _inProgressEvents);
                }

                return _inProgressEvents;
            }
        }

        private ObservableCollection<Event> _doneEvents;

        public ObservableCollection<Event> DoneEvents
        {
            get
            {
                if (_doneEvents == null)
                {
                    _doneEvents = new ObservableCollection<Event>();

                    AssignEventsByStatus(EventStatus.Done, _doneEvents);
                }

                return _doneEvents;
            }
        }

        private void AssignEventsByStatus(EventStatus status, ObservableCollection<Event> listToFill)
        {
            var events = DBHelper.ListOfEvents.FindAll(n=>n.Status == status);

            foreach (var eventItem in events)
            {
                listToFill.Add(eventItem);
            }
        }

        private void ShowAddEventView()
        {
            BaseView.SelectedViewModel = new AddEventViewModel(BaseView);
        }
    }
}
