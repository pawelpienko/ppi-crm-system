﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;
using CRMSystem.ViewModels.AllProductsViewModels;
using CRMSystem.ViewModels.CustomerViewModels;

namespace CRMSystem.ViewModels
{
    public class AddEventViewModel : BaseViewModel
    {
        private Event _eventModel;
        private Task _selectedTask;
        private Product _selectedProduct;
        public AddEventViewModel(BaseViewModel view, Cubes currentCube = Cubes.EventPanel) : base(view)
        {
            Initialize();
            CurrentCube = currentCube;
            _eventModel = new Event();
            SetSummaryPrice(_eventModel);
        }

        public string SummaryPrice { get; set; }

        private void SetSummaryPrice(Event currentEvent)
        {
            if (currentEvent == null)
            {
                SummaryPrice = string.Empty;
                return;
            }

            var summary = currentEvent.Products.Sum(n => n.Price);
            SummaryPrice = $"Summary: {summary.ToString("0.00", CultureInfo.CurrentCulture)} zł";
        }

        private void Initialize()
        {
            AddNewEventCommand = new RelayCommand(AddEvent);
            AddNewTaskCommand = new RelayCommand(AddTask);
            DiscardChangesCommand = new RelayCommand(DiscardChanges);
            RemoveTaskCommand = new RelayCommand(RemoveTask);
            RemoveProductCommand = new RelayCommand(RemoveProduct);
            AddNewProductCommand = new RelayCommand(ShowProductsView);
        }

        public AddEventViewModel(BaseViewModel view, Event currentEvent) : base(view)
        {
            Initialize();
            _eventModel = currentEvent;
            SetSummaryPrice(_eventModel);
        }

        private Customer _selectedCustomer;
        public Customer SelectedCustomer
        {
            get
            {
                if (_selectedCustomer == null)
                {
                    _selectedCustomer = _eventModel.EventCustomer;
                }
                return _selectedCustomer;

            }
            set { _selectedCustomer = value; }
        }


        public ObservableCollection<Product> SelectedProducts
        {
            get
            {
                return _eventModel.Products;

            }
            set
            {
                _eventModel.Products = value;
            }
        }


        #region ICommand members and methods

        public ICommand AddNewEventCommand { get; set; }
        public ICommand DiscardChangesCommand { get; set; }
        public ICommand AddNewTaskCommand { get; set; }
        public ICommand RemoveTaskCommand { get; set; }
        public ICommand AddNewProductCommand { get; set; }
        public ICommand RemoveProductCommand { get; set; }

        private ObservableCollection<Customer> _customers;
        public ObservableCollection<Customer> Customers
        {
            get
            {
                if (_customers == null)
                {
                    _customers = new ObservableCollection<Customer>();
                    var allCustomers = DBHelper.ListOfCustomers;
                    foreach (var customer in allCustomers)
                    {
                        _customers.Add(customer);
                    }
                }

                return _customers;
            }
            set
            {
                _customers = value;
            }
        }

        private void AddEvent()
        {
            _eventModel.EventCreationDate = DateTime.Now;
            if (_eventModel.EventExecutionDate == DateTime.MinValue)
            {
                _eventModel.EventExecutionDate = DateTime.Now;
            }

            var time = _eventModel.EventExecutionDate.Ticks + SelectedTime.Ticks;
            var dateTime = new DateTime(time).AddHours(-12);
            _eventModel.EventExecutionDate = dateTime;

            _eventModel.EventCustomer = SelectedCustomer;
            _eventModel.ExecutionDateWithDesc = $"Execution date: {_eventModel.EventExecutionDate.ToString("dddd, dd MM yyyy", CultureInfo.InvariantCulture)}";
            DBHelper.AddNewEvent(_eventModel);
            BackToMainPanel();
        }

        private void RemoveTask()
        {
            if (_selectedTask != null)
            {
                var tasks = _eventModel.Tasks;
                tasks.Remove(_selectedTask);
            }
        }

        private void DiscardChanges()
        {
            BackToMainPanel();
        }
        
        private void AddTask()
        {
            _eventModel.Tasks.Add(new Task());
        }

        private void ShowProductsView()
        {
            BaseView.SelectedViewModel = new AllProductsViewModel(BaseView, _eventModel);
        }

        private void RemoveProduct()
        {
            if (_selectedProduct != null)
            {
                var products = _eventModel.Products;
                products.Remove(_selectedProduct);
                SetSummaryPrice(_eventModel);
            }
        }

        private void BackToMainPanel()
        {
            BaseViewModel mainPanel;
            switch (CurrentCube)
            {
                case Cubes.Customers:
                    mainPanel = new AllCustomersViewModel(BaseView);
                    break;
                default:
                    mainPanel = new EventPanelViewModel(BaseView);
                    break;
            }

            BaseView.SelectedViewModel = mainPanel;
        }

        #endregion

        #region properties

        public DateTime SelectedTime { get; set; }

        public Task SelectedTask
        {
            get { return null; }
            set { _selectedTask = value; }
        }

        public Product SelectedProduct
        {
            get { return null; }
            set { _selectedProduct = value; }
        }

        public ObservableCollection<Task> Tasks
        {
            get { return _eventModel.Tasks; }
            set
            {
                
            }
        }

        public string EventTitle
        {
            get { return _eventModel.EventTitle; }
            set { _eventModel.EventTitle = value; }

        }

        public string Description
        {
            get { return _eventModel.Description; }
            set { _eventModel.Description = value; }

        }

        public DateTime EventExecutionDate
        {
            get
            {
                return _eventModel.EventExecutionDate;
            }
            set
            {
                _eventModel.EventExecutionDate = value;
            }
        }

        public EventStatus Status
        {
            get
            {
                return _eventModel.Status;

            }
            set
            {
                _eventModel.Status = value;

            }

        }

        public IEnumerable<EventStatus> EventStatuses
        {
            get
            {
                return Enum.GetValues(typeof(EventStatus)).Cast<EventStatus>();
            }
        }


        #endregion
    }
}
