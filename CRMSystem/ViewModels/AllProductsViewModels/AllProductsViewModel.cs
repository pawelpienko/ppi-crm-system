﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.Models;

namespace CRMSystem.ViewModels.AllProductsViewModels
{
    public class AllProductsViewModel : BaseViewModel
    {
        public ICommand RemoveProductCommand { get; set; }
        public ICommand AddProductCommand { get; set; }
        public ICommand EnterSearchInputCommand { get; set; }
        public ICommand DiscardChangesCommand { get; set; }
        public ICommand SaveChangesCommand { get; set; }
        private Event _currentEvent;
        private BaseViewModel _baseView;
        public AllProductsViewModel(BaseViewModel view, Event currentEvent, Cubes currentCube = Cubes.EventPanel) : base(view)
        {
            _baseView = view;
            RemoveProductCommand = new RelayCommand(RemoveProduct);
            AddProductCommand = new RelayCommand(AddProduct);
            EnterSearchInputCommand = new RelayCommand(EnterSearchInput);
            DiscardChangesCommand = new RelayCommand(DiscardChangesProduct);
            SaveChangesCommand = new RelayCommand(SaveChangesProduct);
            _currentEvent = currentEvent;
            CurrentCube = currentCube;
        }

        private string _searchInput;
        public string SearchInput
        {
            get
            {
                return _searchInput;

            }
            set
            {
                _searchInput = value;
                if (string.IsNullOrEmpty(value))
                {
                    EnterSearchInput();
                }
            }
        }

        private void SaveChangesProduct()
        {
            _currentEvent.Products = SelectedProducts;
            //BaseView.SelectedViewModel = new AddEventViewModel(_baseView, _currentEvent);
            BackToMainPanel();
        }

        private void DiscardChangesProduct()
        {
            // BaseView.SelectedViewModel = new AddEventViewModel(_baseView, _currentEvent);
            BackToMainPanel();
        }

        private void BackToMainPanel()
        {
            BaseViewModel mainPanel;
            switch (CurrentCube)
            {
                case Cubes.EventEdit:
                    mainPanel = new EditEventViewModel(BaseView, _currentEvent);
                    break;
                default:
                    mainPanel = new AddEventViewModel(BaseView, _currentEvent);
                    break;
            }

            BaseView.SelectedViewModel = mainPanel;
        }


        private void EnterSearchInput()
        {
            var selected = DBHelper.ListOfProducts.Where(n => n.ProductName.ToLower().Contains(SearchInput.ToLower())).ToList();
            AllProducts = new ObservableCollection<Product>(selected);
        }

        private void RemoveProduct()
        {
            if (ProductToRemove != null)
            {
                var product = SelectedProducts.FirstOrDefault(n => n.ProductId == ProductToRemove.ProductId);
                SelectedProducts.Remove(product);
            }
        }

        private void AddProduct()
        {
            if (ProductToAdd != null && SelectedProducts.FirstOrDefault(n=>n.ProductId == ProductToAdd.ProductId) == null)
            {
                SelectedProducts.Add(ProductToAdd);
            }
        }

        public Product ProductToRemove { get; set; }

        public Product ProductToAdd { get; set; }

        private ObservableCollection<Product> _allProducts;

        public ObservableCollection<Product> AllProducts
        {
            get
            {
                if (_allProducts == null)
                {
                    _allProducts = new ObservableCollection<Product>();
                    var products = DBHelper.ListOfProducts;

                    foreach (var product in products)
                    {
                        _allProducts.Add(product);    
                    }
                }

                return _allProducts;
            }
            set { _allProducts = value; }
        }

        private ObservableCollection<Product> _selectedProducts;

        public ObservableCollection<Product> SelectedProducts
        {
            get
            {
                if (_selectedProducts == null)
                {
                    var products = _currentEvent.Products;
                    _selectedProducts = new ObservableCollection<Product>();
                    foreach (var product in products)
                    {
                        _selectedProducts.Add(product);
                    }
                }
                return _selectedProducts;
            }
            set { _selectedProducts = value; }
        }
    }
}