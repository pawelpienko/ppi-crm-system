﻿using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;

namespace CRMSystem.ViewModels
{
    public class LogginViewModel : BaseViewModel
    {
        public string Login { get; set; }
        public string Password { private get; set; }

        public LogginViewModel(BaseViewModel view): base(view)
        {
            LogginCommand = new RelayCommand(Loggin);
        }

        #region ICommand members and methods

        public ICommand LogginCommand { get; set; }
        public string Message { get; set; }
        private void Loggin()
        {
            var user = DBHelper.LogIn(Login, Password, out string msg, out bool success);
            if (success)
            {
                CurrentUser = user;
                BaseView.CanAccessChildViews = true;
                BaseView.SelectedViewModel = new AccountViewModel(BaseView);
            }

            Message = msg;
        }

        #endregion
    }
}
