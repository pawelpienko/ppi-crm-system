﻿using System.ComponentModel;
using CRMSystem.Helpers;
using CRMSystem.Models;
using PropertyChanged;

namespace CRMSystem.ViewModels
{
    [ImplementPropertyChanged]
    public class BaseViewModel : INotifyPropertyChanged
    {
        protected Cubes CurrentCube;
        public BaseViewModel()
        {
            
        }
        public BaseViewModel(BaseViewModel view)
        {
            BaseView = view;
            AdminModeEnabled = CurrentUser?.Role == UserRoles.Administrator;
        }
        public bool AdminModeEnabled { get; set; }

        private static User _currentUser;
        public static User CurrentUser
        {
            get
            {
                //if (_currentUser == null)
                //{
                //    _currentUser = DBHelper.GetUserTest();
                //}

                return _currentUser;
            }
            set { _currentUser = value; }
        }

        public bool CanAccessChildViews { get; set; }
        
        private BaseViewModel _selectedViewModel;

        public BaseViewModel SelectedViewModel
        {
            get { return _selectedViewModel; }
            set { _selectedViewModel = value; }
        }


        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public BaseViewModel BaseView { get; set; }
    }
}
