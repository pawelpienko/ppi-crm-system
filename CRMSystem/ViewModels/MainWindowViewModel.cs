﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using CRMSystem.Commands;
using CRMSystem.Helpers;
using CRMSystem.PdfDocumentManager;
using CRMSystem.ViewModels.ChartsViewModels;
using CRMSystem.ViewModels.SupplierViewModels;
using Tulpep.NotificationWindow;

namespace CRMSystem.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        public ICommand ShowAccountViewCommand { get; set; }
        public ICommand ShowEventPanelViewCommand { get; set; }
        public ICommand ShowCalendarViewCommand { get; set; }
        public ICommand ShowCustomersViewCommand { get; set; }
        public ICommand ShowAllSuppliersViewCommand { get; set; }
        public ICommand ShowChartsViewCommand { get; set; }

        public MainWindowViewModel()
        {
            CanAccessChildViews = false;
            ShowAccountViewCommand = new RelayCommand(OpenAccountView);
            ShowEventPanelViewCommand = new RelayCommand(OpenEventPanelView);
            ShowCalendarViewCommand = new RelayCommand(OpenCalendarView);
            ShowCustomersViewCommand = new RelayCommand(OpenCustomersView);
            ShowChartsViewCommand = new RelayCommand(OpenChartsView);
            ShowAllSuppliersViewCommand = new RelayCommand(OpenAllSuppliersView);
            SelectedViewModel = new LogginViewModel(this);
            new NotificationHelper().Initialize();

        }

        private void OpenAccountView()
        {
            SelectedViewModel = new AccountViewModel(this);
        }

        private void OpenCalendarView()
        {
            SelectedViewModel = new CalendarViewModel(this);
        }

        private void OpenEventPanelView()
        {
            SelectedViewModel = new EventPanelViewModel(this);
        }

        private void OpenCustomersView()
        {
            SelectedViewModel = new AllCustomersViewModel(this);
        }

        private void OpenChartsView()
        {
            SelectedViewModel = new ChartsViewModel(this);
        }

        private void OpenAllSuppliersView()
        {
            SelectedViewModel = new AllSuppliersViewModel(this);
        }

    }

    public enum Cubes
    {
        EventPanel,
        EventEdit,
        Calendar,
        Customers,
        Account
    }
}
